<?php
/* Template Name: Landing test */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>



    <!-- Main Container Starts -->
    <main class="main-container new-home">

<div class="g-logo-wrap">
            <div class="g-logo">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/g-logo.svg" alt="">
            </div>
        </div>
        <!-- <div class="gsap-dot dot-1"></div>
        <div class="gsap-dot dot-2"></div>
        <div class="gsap-dot dot-3"></div>
        <div class="gsap-dot dot-4"></div>
        <div class="gsap-dot dot-5"></div>
        <div class="gsap-dot dot-6"></div> -->

        <!-- banner -->
        <section class="banner-sec">

            <div class="banner-height">
                <div class="banner-trigg banner-1"></div>
                <div class="banner-trigg banner-2"></div>
                <div class="banner-trigg banner-3"></div>
                <div class="banner-trigg banner-4"></div>
                <!-- <div class="banner-trigg banner-5"></div> -->
                <!-- <div class="banner-trigg banner-6"></div> -->
                <div class="banner-logo-trigger"></div>
                <!-- <div class="banner-trigg banner-7"></div> -->
                <div class="banner-sticky">

                    <div class="banner-wrap">
                        <div class="home-banner-video">

                            <video src="<?php bloginfo('template_url'); ?>/assets/video/banner-video.mp4" loop autoplay muted playsinline
                                referrerpolicy="strict-origin-when-cross-origin"></video>

                            <a href="https://www.youtube.com/watch?v=OmvMrME36HM?playlist=OmvMrME36HM&loop=1&rel=0&fs=0&iv_load_policy=3&playsinline=1&autoplay=1&mute=1"
                                class="
                                comm-video-btn comm-yt-popup">
                                <div class="play-button">
                                    <h5 class="large-title">Play</h5>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/play-btn.svg" alt="" width="100%" height="100%">
                                </div>
                            </a>
                        </div>

                        <div class="container">

                            <div class="mob-content">

                                <!-- banner content -->
                                <div class="comm-section">
                                    <div class="mob-banner-content">
                                        <h1 class="banner-title">Your partner in process analytics and technical
                                            consulting
                                        </h1>

                                        <div class="banner-subdscr">
                                            <a href="<?php echo get_site_url().'/contact'?>" class="button">Contact Us</a>
                                            <div class="comm-para">
                                                <p>Engineering Expertise and Digital Transformation Solutions for
                                                    Increased
                                                    Production,
                                                    Sustainability, Efficiency and Safety.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- five card -->
                                <div class="comm-section">
                                    <div class="f-row f-3 f-768-2 f-480-1 mob-five-card">
                                        <div class="f-col">
                                            <a href="<?php echo get_site_url().'/sustainable'?>">
                                                <div class="five-card">
                                                    <h3>Zero Carbon Pathways</h3>
                                                    <div class="five-img">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f1.png" alt="">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="f-col">
                                            <a href="<?php echo get_site_url().'/apply-ai'?>">
                                                <div class="five-card">
                                                    <h3>Applied AI</h3>
                                                    <div class="five-img">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f2.png" alt="">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="f-col">
                                            <a href="<?php echo get_site_url().'/reliability'?>">
                                                <div class="five-card">
                                                    <h3>Asset performance management</h3>
                                                    <div class="five-img">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f3.png" alt="">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="f-col">
                                            <a href="<?php echo get_site_url().'/engineering-service'?>">
                                                <div class="five-card">
                                                    <h3>Design Engineering</h3>
                                                    <div class="five-img">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f4.png" alt="">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="f-col">
                                            <a href="<?php echo get_site_url().'/safety-service'?>">
                                                <div class="five-card">
                                                    <h3>Safety + Risk Analysis</h3>
                                                    <div class="five-img">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f5.png" alt="">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <!-- g logo -->
                                <div class="comm-section">
                                    <div class="mob-g-logo">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/g-logo.svg" alt="">
                                    </div>
                                </div>

                            </div>

                            <div class="five-card-wrap banner-card-wrap">
                                <div class="f-row f-3">
                                    <div class="f-col banner-card card-1">
                                        <a href="<?php echo get_site_url().'/sustainable'?>">
                                            <div class="five-card">
                                                <h3>Zero Carbon Pathways</h3>
                                                <div class="five-img">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/f1.png" alt="">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="f-col banner-card card-2">
                                        <a href="<?php echo get_site_url().'/apply-ai'?>">
                                            <div class="five-card">
                                                <h3>Applied AI</h3>
                                                <div class="five-img">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/f2.png" alt="">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="f-col banner-card card-3">
                                        <a href="<?php echo get_site_url().'/reliability'?>">
                                            <div class="five-card">
                                                <h3>Asset performance management</h3>
                                                <div class="five-img">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/f3.png" alt="">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="f-col banner-card card-4">
                                        <a href="<?php echo get_site_url().'/engineering-service'?>">
                                            <div class="five-card">
                                                <h3>Design Engineering</h3>
                                                <div class="five-img">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/f4.png" alt="">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="f-col banner-card card-5">
                                        <a href="<?php echo get_site_url().'/safety-service'?>">
                                            <div class="five-card">
                                                <h3>Safety + Risk Analysis</h3>
                                                <div class="five-img">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/f5.png" alt="">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="banner-content">
                                    <h1 class="banner-title">Your partner in process analytics and technical
                                        consulting
                                        <span class="banner-dot banner-dot-1"></span>
                                        <span class="banner-dot banner-dot-2"></span>
                                        <span class="banner-dot banner-dot-3"></span>
                                    </h1>

                                    <div class="banner-subdscr">
                                        <a href="<?php echo get_site_url().'/contact'?>" class="button">Contact Us</a>
                                        <div class="comm-para">
                                            <p>Engineering Expertise and Digital Transformation Solutions for
                                                Increased
                                                Production,
                                                Sustainability, Efficiency and Safety.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>

        <!-- text animation -->
        <div class="orange-bg-sec-wrap">
            <div class="show-orange-sec-trigger"></div>
            <section class="comm-section anim-sec orange-bg-sec">
                <!-- <div class="fold3-dot-wrap"> -->
                <div class="fold3-dot fold3-dot-1"></div>
                <div class="fold3-dot fold3-dot-2"></div>
                <div class="fold3-dot fold3-dot-3"></div>
                <div class="fold3-dot fold3-dot-4"></div>
                <!-- </div> -->
                <div class="container">
                    <div class="anim-text-box">
                        <h5 class="anim-text">
                            We deliver innovative engineering solutions and digital transformation
                            services for clients in
                            the energy and chemical industries.
                        </h5>
                    </div>
                </div>
            </section>
        </div>

        <!-- services -->
        <!-- <section class="comm-section serv-sec">
            <div class="container">
                <div class="service-wrap">
                    <h2 class="sec-title t-center">Ingenero in numbers</h2>

                    <div class="f-row f-3 f-768-1">
                        <div class="f-col">
                            <div class="service-small-card l-orange">
                                <h3 class="counter-value">
                                    <span class="counterValue" data-count="20">0</span><span>M+</span>
                                </h3>
                                <div class="counter-para">
                                    <p>Data Points Analysed Daily</p>
                                </div>
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="service-small-card l-orange">
                                <h3 class="counter-value">
                                    <span class="counterValue" data-count="10">0</span><span>M+</span>
                                </h3>
                                <div class="counter-para">
                                    <p>Process Engineering Manhours</p>
                                </div>
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="service-small-card l-orange">
                                <h3 class="counter-value">
                                    <span class="counterValue" data-count="1.6">0</span><span>K+</span>
                                </h3>
                                <div class="counter-para">
                                    <p>Process Studies Conducted</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="f-row f-2 f-768-1">
                        <div class="f-col">
                            <div class="service-small-card  lblue-bg">
                                <div class="counter-row">
                                    <h3 class="counter-value">
                                        <span class="counterValue" data-count="100">0</span><span>K+</span>
                                    </h3>
                                    <div class="counter-para">
                                        <p>Relief Devices Handles</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="f-col">
                            <div class="service-small-card lblue-bg">
                                <div class="counter-row">
                                    <h3 class="counter-value">
                                        <span class="counterValue" data-count="500">0</span><span>+</span>
                                    </h3>
                                    <div class="counter-para">
                                        <p>Applied AI Solutions Use Cases</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="service-big-card l-yellow">
                        <div class="counter-para">
                            <p>Value Capture from Digital Solutions</p>
                        </div>
                        <div class="f-row f-3 f-640-2">
                            <div class="f-col">
                                <h3 class="counter-value">
                                    <span>></span> <span class="counterValue" data-count="18">0</span><span>%</span>
                                </h3>
                                <div class="value-para">
                                    <p>Production</p>
                                </div>
                            </div>
                            <div class="f-col">
                                <h3 class="counter-value">
                                    <span>></span> <span class="counterValue" data-count="25">0</span><span>%</span>
                                </h3>
                                <div class="value-para">
                                    <p>Emission Reduction</p>
                                </div>
                            </div>
                            <div class="f-col">
                                <h3 class="counter-value">
                                    <span>></span> <span class="counterValue" data-count="12">0</span><span>%</span>
                                </h3>
                                <div class="value-para">
                                    <p>Energy Savings</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

         <!-- services sticky -->
        <section class="comm-section">
            <div class="container">

                <div class="safety-study-content-wrap">

                    <div class="sticky-cont-wrapper">
                        <div class="safety-content-left" id="safety">

                            <div class="sticky-content">
                                <h2 class="sec-title text-semiBold">Ingenero in<br /> numbers</h2>
                                <!-- <div class="comm-para">
                                    <p>Ingenero has decades of experience helping you ensure that your operations are
                                        the
                                        safest
                                        that they can be. We have carried out hundreds of studies in the following
                                        categories:
                                    </p>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="safety-content-right numbers-sticky">

                        <div class="white-box">
                            <h2 class="sec-title">20M+</h2>

                            <div class="large-title text-semiBold">
                                <p>Data Points Analysed Daily</p>
                            </div>
                        </div>
                        <div class="white-box">
                            <h2 class="sec-title">10M+</h2>

                            <div class="large-title text-semiBold">
                                <p>Process Engineering Manhours</p>
                            </div>
                        </div>
                        <div class="white-box">
                            <h2 class="sec-title">1.6K+</h2>

                            <div class="large-title text-semiBold">
                                <p>Process Studies Conducted</p>
                            </div>
                        </div>
                        <div class="white-box">
                            <h2 class="sec-title">100K+</h2>

                            <div class="large-title text-semiBold">
                                <p>Relief Devices Handles</p>
                            </div>
                        </div>
                        <div class="white-box">
                            <h2 class="sec-title">500+</h2>

                            <div class="large-title text-semiBold">
                                <p>Applied AI Solutions Use Cases</p>
                            </div>
                        </div>
                        <div class="white-box value-number-box">

                            <div class="large-title text-semiBold">
                                <p>Value Capture from Digital Solutions</p>
                            </div>

                            <div class="value-row">
                                <h2 class="sec-title">>18% </h2><span>Production</span>
                            </div>
                            <div class="value-row">
                                <h2 class="sec-title">>25% </h2><span>Emission Reduction</span>
                            </div>
                            <div class="value-row">
                                <h2 class="sec-title">>12% </h2><span>Energy Savings</span>
                            </div>
                        </div>


                    </div>


                </div>

            </div>
        </section>

        <!-- industries  -->
        <section class="comm-section ind-sec">
            <div class="container">
                <div class="industries-wrap">
                    <div class="industries-left">

                        <div class="swiper indNameSwiper">
                            <div class="swiper-wrapper swiper-linear">
                                <div class="swiper-slide">
                                    <h3 class="industries-name">Refining</h3>
                                </div>
                                <div class="swiper-slide">
                                    <h3 class="industries-name">Oil and Gas</h3>
                                </div>
                                <div class="swiper-slide">
                                    <h3 class="industries-name">Mid-Stream Gas<br /> Processing</h3>
                                </div>
                                <div class="swiper-slide">
                                    <h3 class="industries-name">Oil and Gas</h3>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                    <div class="industries-right">
                        <div class="swiper indDetailSwiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="industries-detail">
                                        <h2 class="sec-title">Industries We Serve</h2>
                                        <div class="comm-para">
                                            <p>At Ingenero, we have extensive experience working with clients across a
                                                range of industries</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="industries-detail">
                                        <h2 class="sec-title">Industries We Serve</h2>
                                        <div class="comm-para">
                                            <p>At Ingenero, we have extensive experience working with clients across a
                                                range of industries</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="industries-detail">
                                        <h2 class="sec-title">Industries We Serve</h2>
                                        <div class="comm-para">
                                            <p>At Ingenero, we have extensive experience working with clients across a
                                                range of industries</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="industries-detail">
                                        <h2 class="sec-title">Industries We Serve</h2>
                                        <div class="comm-para">
                                            <p>At Ingenero, we have extensive experience working with clients across a
                                                range of industries</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- wordcloud -->
         <section class="comm-section">
            <div class="container">
                <h2 class="sec-title t-center">Our Partners</h2>
                <!-- <div class="wordcloud-img">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/wordcloud.png" alt="">
                </div> -->
                <div class="partner-wrap">
                    <div class="f-row f-6 f-1200-5 f-990-4 f-640-3">
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p1.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p2.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p3.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p4.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p5.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p6.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p7.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p8.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p9.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p10.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/p11.png" alt="">
                            </div>
                        </div>
                        <div class="f-col">
                            <div class="partner-img">
                                <p>and <br/>more...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- more study boxes sec -->
        <!-- <section class="comm-section knowledge-sec">
            <div class="container">
                <div class="sec-header">
                    <h2 class="sec-title">Knowledge Base</h2>
                    <a href="knowledge-base.html" class="button line">View All</a>
                </div>
                <div class="swiper caseSwiper case-swiper">
                    <div class="swiper-wrapper">

                        <div class="swiper-slide">
                            <a href="pdf/Oil & Gas/Oil & Gas Case Use Case.pdf" download="">
                                <div class="cs-swiper-box">
                                    <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt=""></div>
                                    <div class="css-swiper-content">
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Oil & Gas Case Use Case 1</h3>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide">
                            <a href="pdf/Petrochemicals/Petrochemicals Use Case1.pdf" download>
                                <div class="cs-swiper-box">
                                    <div class="css-swiper-content">
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use Case 1 </h3>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </div>

                        <div class="swiper-slide">
                            <a href="pdf/Power & Utilities/Power & Utilities Case Study1.pdf" download>
                                <div class="cs-swiper-box">
                                    <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                    <div class="css-swiper-content">
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Power & Utilities Case Study 1 </h3>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>


                    </div>
                </div>
                <a href="knowledge-base.html" class="button line mob-btn">View All</a>

            </div>
        </section> -->

        <!-- use case -->
        <section class="comm-section">
            <div class="container">
                <div class="usecase-wrap">
                    <div class="section-heading">
                        <h2 class="sec-title t-center ">Use Cases</h2>
                    </div>

                    <div class="usecase-gallery">
                        <div class="usecase-card active">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                <h3 class="usecase-title">Oil and Gas</h3>

                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#oil">
                                            <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Oil and Gas</h3>
                            </div>
                        </div>
                        <div class="usecase-card">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <h3 class="usecase-title">Petrochemical</h3>
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#petro">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Petrochemical</h3>
                            </div>
                        </div>
                        <div class="usecase-card">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <h3 class="usecase-title">Refining</h3>
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#refine">
                                            <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Refining</h3>
                            </div>
                        </div>
                        <div class="usecase-card">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <h3 class="usecase-title">Power And Utilities</h3>
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#power">
                                            <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Power And Utilities</h3>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>



    </main>
    <!-- Main Container Ends -->

<?php get_footer(); ?>


<script>
        $(".usecase-card").hover(function () {
            $(".usecase-card").removeClass("active")
            $(this).addClass("active")
        })

        if (window.matchMedia("(max-width: 1024px)").matches) {
            $('.usecase-card').addClass('active')
        }

        /* digital capability sec script */
        var digitalSwiper = new Swiper(".digitalSwiper", {
            spaceBetween: 30,
            slidesPerView: 2.5,
            speed: 800,
            simulateTouch: false,
            allowTouchMove: false,

            scrollbar: {
                el: ".swiper-scrollbar",
            },

            breakpoints: {
                360: {
                    slidesPerView: 1,
                    allowTouchMove: true,
                },
                577: {
                    slidesPerView: 2,
                },
                1025: {
                    slidesPerView: 3,
                },
            },


        });

        let mm = gsap.matchMedia();

        mm.add("(min-width: 577px)", () => {

            const offerTxt = document.querySelectorAll(".trigger-point");
            offerTxt.forEach((section, i) => {
                const tlofferTxt = gsap.timeline({
                    scrollTrigger: {
                        trigger: section,
                        start: "0% 20%",
                        end: "0% 20%",
                        toggleActions: "play none none reverse",
                        onEnter: () => techSlideEnter(i),
                        onEnterBack: () => techSlideEnterBack(i),
                        // markers: true
                    },
                });
            });

            function techSlideEnter(i) {
                digitalSwiper.slideTo(i);
            }

            function techSlideEnterBack(i) {
                digitalSwiper.slideTo(i - 1);
            }

            var swiperTxtGroupCount = document.querySelectorAll(".swiperTxtGroup").length;

            $(".digitals-height-wrap").css({
                "height": (swiperTxtGroupCount * 80) + "vh"
            });

        })

        /* digital page glance sec script */

        // banner youtube popup
    $(document).ready(function () {
        $('.comm-yt-popup').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    });
</script>


