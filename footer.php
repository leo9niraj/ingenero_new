<footer id="footer">



    <div id="business"></div>

    <div class="container">
        <div class="footer-top">
            <!-- <div class="footer-logo">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/logo.svg" alt="">
            </div>
            <div class="footer-mob-logo">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/footer-mob-logo.svg" alt="">
            </div>
            <div class="footer-link">
                <ul>
                    <li><a href="<?php echo get_site_url().'/about'?>">About Us</a></li>
                    <li><a href="<?php echo get_site_url().'/sustainable'?>">Solutions</a></li>
                    <li><a href="<?php echo get_site_url().'/contact'?>">Contact</a></li>           
                    <li><a href="<?php echo get_site_url().'/blog'?>">Knowledge Base</a></li>
                </ul>
            </div> -->
            <!-- <div class="footer-social">
                <div class="social-img">
                    <a href="https://www.linkedin.com/company/ingenero/" target="blank">
                        <i class="icon-li"></i>
                    </a>
                </div>
            </div> -->
        </div>
        <hr>
         <div class="footer-bottom">
            <div class="footer-left">
                <div class="small-para">
                    <p>© 2024 Ingenero</p>
                </div>
            </div>
            
            <div class="footer-right">
                <ul>
                    <li><a href="<?php echo get_site_url().'/our-terms-of-service'?>">Terms</a></li>
                    <li><a href="<?php echo get_site_url().'/privacy-policy'?>">Privacy</a></li>
                </ul>
               
            </div>
             <div class="footer-social">
                <div class="social-img">
                    <a href="https://www.linkedin.com/company/ingenero/" target="blank">
                        <i class="icon-li"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>


</body>

</html>