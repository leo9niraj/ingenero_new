<?php
/* Template Name: Safety service */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); 

?>

<!-- Main Container Starts -->
<div class="main-container">
    <?php $banner_section_data = get_field('banner_section');
                if ($banner_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="page-hdr">
                <div class="f-row">
                    <div class="w40 w-990-45 w-834-60 w-576-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title"><?php the_title()?></h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p><?php the_title()?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-576-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p><?php echo $banner_section_data['sub_title'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-bnr">
                <img src="<?php echo $banner_section_data['banner_image'] ?>" alt="">
            </div>


            <div class="page-big-content">
                <h2 class="large-title"><?php echo $banner_section_data['content'] ?></h2>
            </div>
            <div class="page-bnr page-bnr-1">
                <img src="<?php echo $banner_section_data['banner_image_2'] ?>" alt="">
            </div>
        </div>
    </section>
    <?php endif; ?>




    <!-- Relief System section -->
    <?php $system_engineering_data = get_field('system_engineering');
        if ($system_engineering_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="safety-study-content-wrap" id="rel">
                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left" >
                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold"><?php echo $system_engineering_data['main_title'] ?>
                            </h2>
                            <div class="comm-para">
                                <p><?php echo $system_engineering_data['sub_title'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="safety-content-right">
                    <div class="relief-cont-wrap">
                        <div class="relief-cont">
                            <div class="med-para">
                                <p class="text-semiBold"><?php echo $system_engineering_data['title_1'] ?></p>
                            </div>
                            <?php echo $system_engineering_data['content_1'] ?>
                        </div>
                        <div class="relief-cont">
                            <div class="med-para">
                                <p class="text-semiBold">
                                    <?php echo $system_engineering_data['title_2'] ?></p>
                            </div>
                            <?php echo $system_engineering_data['content_2'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- Relief System section End -->

    <!-- safety services section -->
    <?php $safety_studies_data = get_field('safety_studies');
                if ($safety_studies_data) : ?>
    <section class="comm-section" >
        <div class="container">
            <div class="safety-study-content-wrap" id="safety">
                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left" >
                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold"><?php echo $safety_studies_data['title'] ?></h2>
                            <div class="comm-para">
                                <p><?php echo $safety_studies_data['sub_title'] ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="safety-content-right">
                    <?php $i = 1;
                     if (have_rows('safety_studies')) : while (have_rows('safety_studies')) : the_row();
                    if (have_rows('safety_studies_cards')) : while (have_rows('safety_studies_cards')) : the_row(); ?>
                    <div class="white-box">
                        <h2 class="small-title text-medium"><?php the_sub_field('title')?></h2>

                        <div class="safety-cont-para med-para">
                            <p><?php the_sub_field('sub_title')?></p>
                        </div>
                    </div>
                    <?php $i++; 
                    endwhile;endif;
                endwhile;endif?>
                    <!-- <div class="white-box">
                        <h2 class="small-title text-medium">HAZID <br> (Hazard Identification)</h2>

                        <div class="safety-cont-para med-para">
                            <p>An initial assessment to identify potential hazards and risks in a project or
                                process.</p>
                        </div>
                    </div>

                    <div class="white-box">
                        <h2 class="small-title text-medium">QRA <br> (Quantitative Risk Assessment)</h2>

                        <div class="safety-cont-para med-para">
                            <p>A detailed analysis that quantifies risks and their probabilities, often used to
                                prioritize safety measures.</p>
                        </div>
                    </div>

                    <div class="white-box">
                        <h2 class="small-title text-medium">SIL <br> (Safety Integrity Level)</h2>

                        <div class="safety-cont-para med-para">
                            <p>A measure of safety system effectiveness to prevent or mitigate hazardous events.</p>
                        </div>
                    </div>

                    <div class="white-box">
                        <h2 class="small-title text-medium">LOPA <br> (Layer of Protection Analysis)</h2>

                        <div class="safety-cont-para med-para">
                            <p>A method to assess the effectiveness of layers of protection and determine if they
                                meet safety requirements.</p>
                        </div>
                    </div> -->
                </div>


            </div>

        </div>
    </section>
    <?php endif; ?>
    <!-- safety services section End -->

    <!-- Codename SECURUS section -->
    <?php $securus_section_data = get_field('securus_section');
        if ($securus_section_data) : ?>
    <section class="comm-section" >
        <div class="container">

            <div class="safety-study-content-wrap" id="securus">

                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left" >

                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold"><?php echo $securus_section_data['main_title'] ?></h2>

                        </div>
                    </div>
                </div>

                <div class="safety-content-right">

                    <div class="relief-cont-wrap">
                        <div class="relief-cont">
                            <div class="med-para">
                                <p class="text-semiBold"><?php echo $securus_section_data['title_1'] ?></p>
                            </div>
                            <?php echo $securus_section_data['content_1'] ?>
                        </div>

                        <div class="relief-cont bottom-space-0">
                            <div class="med-para">
                                <p class="text-semiBold"><?php echo $securus_section_data['title_2'] ?>
                                </p>
                            </div>
                            <?php echo $securus_section_data['content_2'] ?>
                        </div>

                    </div>

                </div>


            </div>

        </div>
    </section>
    <?php endif; ?>

    <!-- learning section -->
    <?php $learning_section_data = get_field('learning_section');
    if ($learning_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="learning-wrap lblue-bg safety-learn">
                <div class="learning-img">
                    <img src="<?php echo $learning_section_data['banner_image'] ?>" alt="">
                </div>
                <div class="learning-content">
                    <h2 class="sec-title"><?php echo $learning_section_data['title'] ?></h2>
                    <div class="comm-para">
                        <p><?php echo $learning_section_data['sub_title'] ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- broad exp section -->
    <?php $broad_experience_section_data = get_field('broad_experience_section');
                            if ($broad_experience_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="section-wrap">
                <div class="section-left counter-broad">
                    <div class="expr-hdr">
                        <h2 class="large-title text-semiBold"><?php echo $broad_experience_section_data['title'] ?></h2>
                    </div>
                    <div class="comm-para">
                        <p><?php echo $broad_experience_section_data['sub_title'] ?></p>
                    </div>
                </div>
                <div class="section-right expr-sec counter">
                    <div class="f-row f-2 f-480-1">
                        <?php $i = 1;
                     if (have_rows('broad_experience_section')) : while (have_rows('broad_experience_section')) : the_row();
                        if (have_rows('counters')) : while (have_rows('counters')) : the_row(); ?>
                        <div class="f-col">
                            <div class="count-box l-orange">
                                <article class="counter-box">
                                    <span class="count plus text-medium"
                                        data-count="<?php the_sub_field('number')?>"><?php the_sub_field('number')?><?php if($i == 1):?>k<?php endif;?></span>
                                </article>
                                <div class="small-para">
                                    <p><?php the_sub_field('title')?></p>
                                </div>
                            </div>
                        </div>
                        <?php $i++; 
                        endwhile;endif;
                    endwhile;endif?>
                        <!-- <div class="f-col">
                            <div class="count-box l-yellow">
                                <article class="counter-box">
                                    <span class="count plus text-medium" data-count="100">000</span>
                                </article>
                                <div class="small-para">
                                    <p>Relief devices validated</p>
                                </div>
                            </div>
                        </div>

                        <div class="f-col">
                            <div class="count-box lblue-bg">
                                <article class="counter-box">
                                    <span class="count plus text-medium" data-count="80">0</span>
                                </article>
                                <div class="small-para">
                                    <p>Relief devices validated</p>
                                </div>
                            </div>
                        </div>

                        <div class="f-col">
                            <div class="count-box l-purple">
                                <article class="counter-box">
                                    <span class="count plus text-medium" data-count="60000">55000</span>
                                </article>
                                <div class="small-para">
                                    <p>Relief devices validated</p>
                                </div>
                            </div>
                        </div> -->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- soft-stdrd section -->
    <?php $software__section_data = get_field('software__section');
     if ($software__section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="sec-hedr t-center">
                <h2 class="sec-title"><?php echo $software__section_data['title'] ?></h2>
                <div class="med-para" id="std">
                    <p><?php echo $software__section_data['sub_title'] ?></p>
                </div>
            </div>
            <div class="soft-stndrd-img-wrap">
                <div class="f-row">
                    <div class="w60 w-834-100">
                        <div class="soft-stndrd-img">
                            <img src="<?php echo $software__section_data['image_1'] ?>" alt="">
                        </div>
                    </div>
                    <div class="w40 w-834-100">
                        <div class="soft-stndrd-img">
                            <img src="<?php echo $software__section_data['image_2'] ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- var proc section -->
    <?php $processes_section_data = get_field('processes_section');
     if ($processes_section_data) : ?>
    <section class="comm-section" id="cover">
        <div class="container">
            <div class="var-procs-sec-wrap" >
                <div class="sec-hedr-left">
                    <h2 class="sec-title"><?php echo $processes_section_data['title'] ?></h2>
                    <div class="med-para">
                        <p><?php echo $processes_section_data['sub_title'] ?>
                        </p>
                    </div>
                </div>
                <div class="var-procs-img">
                    <img src="<?php echo $processes_section_data['image_1'] ?>" alt="">
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>


    <!-- case-study section -->
    <!-- more study boxes sec -->
    <!-- <section class="comm-section knowledge-sec" id="webinar">
        <div class="container">
            <div class="sec-header">
                <h2 class="sec-title">Knowledge Base</h2>
                <a href="knowledge-base.html" class="button line">View All</a>
            </div>
            <div class="swiper caseSwiper case-swiper">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Oil & Gas/Oil & Gas Case Use Case.pdf"
                            download="">
                            <div class="cs-swiper-box">
                                <div class="cs-swiper-img"><img
                                        src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt=""></div>
                                <div class="css-swiper-content">
                                    <div class="cs-swiper-box-btm">
                                        <h3 class="knowledge-card-title">Oil & Gas Case Use Case 1</h3>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case1.pdf"
                            download>
                            <div class="cs-swiper-box">
                                <div class="css-swiper-content">
                                    <div class="cs-swiper-img"><img
                                            src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                    <div class="cs-swiper-box-btm">
                                        <h3 class="knowledge-card-title">Petrochemicals Use Case 1 </h3>
                                    </div>
                                </div>

                            </div>
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Power & Utilities/Power & Utilities Case Study1.pdf"
                            download>
                            <div class="cs-swiper-box">
                                <div class="cs-swiper-img"><img
                                        src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                <div class="css-swiper-content">
                                    <div class="cs-swiper-box-btm">
                                        <h3 class="knowledge-card-title">Power & Utilities Case Study 1 </h3>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <a href="knowledge-base.html" class="button line mob-btn">View All</a>

        </div>
    </section> -->

    <!-- use case -->
    <section class="comm-section">
        <div class="container">
            <div class="usecase-wrap">
                <div class="section-heading">
                    <h2 class="sec-title t-center ">Use Cases</h2>
                </div>

                <div class="usecase-gallery">
                    <div class="usecase-card active">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                            <h3 class="usecase-title">Oil and Gas</h3>

                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#oil">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Oil and Gas</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Petrochemical</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#petro">
                                    <!-- <i class="ph-bold ph-arrow-right"></i> -->
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Petrochemical</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Refining</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#refine">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Refining</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Power And Utilities</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#power">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Power And Utilities</h3>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <?php $pre_footer_section_data = get_field('pre_footer_section');
                if ($pre_footer_section_data) : ?>
    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white"><?php echo $pre_footer_section_data['title'] ?></h2>
                <div class="comm-para t-center white">
                    <p><?php echo $pre_footer_section_data['sub_title'] ?></p>
                </div>
                <a href="<?php echo $pre_footer_section_data['cta'] ?>" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>
<!-- Main Container Ends -->


<?php get_footer(); ?>

<script>
        $(".usecase-card").hover(function () {
            $(".usecase-card").removeClass("active")
            $(this).addClass("active")
        })

        if (window.matchMedia("(max-width: 1024px)").matches) {
            $('.usecase-card').addClass('active')
        }

        /* digital capability sec script */
        var digitalSwiper = new Swiper(".digitalSwiper", {
            spaceBetween: 30,
            slidesPerView: 2.5,
            speed: 800,
            simulateTouch: false,
            allowTouchMove: false,

            scrollbar: {
                el: ".swiper-scrollbar",
            },

            breakpoints: {
                360: {
                    slidesPerView: 1,
                    allowTouchMove: true,
                },
                577: {
                    slidesPerView: 2,
                },
                1025: {
                    slidesPerView: 3,
                },
            },


        });

        let mm = gsap.matchMedia();

        mm.add("(min-width: 577px)", () => {

            const offerTxt = document.querySelectorAll(".trigger-point");
            offerTxt.forEach((section, i) => {
                const tlofferTxt = gsap.timeline({
                    scrollTrigger: {
                        trigger: section,
                        start: "0% 20%",
                        end: "0% 20%",
                        toggleActions: "play none none reverse",
                        onEnter: () => techSlideEnter(i),
                        onEnterBack: () => techSlideEnterBack(i),
                        // markers: true
                    },
                });
            });

            function techSlideEnter(i) {
                digitalSwiper.slideTo(i);
            }

            function techSlideEnterBack(i) {
                digitalSwiper.slideTo(i - 1);
            }

            var swiperTxtGroupCount = document.querySelectorAll(".swiperTxtGroup").length;

            $(".digitals-height-wrap").css({
                "height": (swiperTxtGroupCount * 80) + "vh"
            });

        })

        /* digital page glance sec script */
</script>