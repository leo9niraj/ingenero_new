<?php
/* Template Name: Reliability */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); 

?>

<!-- Main Container Starts -->
<div class="main-container">
 
        <!-- page header -->
        <section class="comm-section">
            <div class="container">
                <div class="page-hdr team-hdr">
                    <div class="f-row">
                        <div class="w40 w-1064-60 w-834-100">
                            <div class="page-hdr-left">
                                <h1 class="banner-title">Asset Performance Management</h1>
                                <div class="breadcrumb">
                                    <ul>
                                        <li>
                                            <a href="">Home</a>
                                        </li>
                                        <li>
                                            <p>Asset Performance Management</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="w40 w-834-100">
                            <div class="page-hdr-right">
                                <div class="comm-para">
                                    <p>INGENERO’s Innovative Solutions for overall Asset Performance Management and
                                        Reliability using Applied AI</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- banner -->
        <section class="rel-banner-sec">
            <div class="container">
                <div class="img-wrap">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/rel-banner.jpg" alt="">
                </div>
                <div class="reliability-content">
                    <h2>In today’s industrial landscape, the reliability of critical assets such as exchangers,
                        compressors, and pumps is paramount for ensuring uninterrupted operations and maximizing
                        productivity.</h2>
                    <div class="med-para">
                        <p>Traditional methods of monitoring and managing these assets often fall short in providing
                            timely insights and predictive capabilities, leading to costly downtimes and
                            inefficiencies. However, with the use of its Applied AI solutions, INGENERO is
                            revolutionizing asset performance monitoring and management, offering unprecedented
                            levels of reliability and efficiency.</p>
                        <p>These solutions help manufacturing units operate more efficiently, maximize their
                            capacities and production rates, lower maintenance and operating cost, reduce plant
                            down times, improve safety and sustainability metrics and increase ROI (i.e.Return
                            On Investment). These solutions can be applied at a single unit or across multiple
                            plants and provide an overview of equipment health.</p>
                    </div>

                </div>
            </div>
        </section>

        <!-- feature -->
        <section class="comm-section">
            <div class="container">
                <div class="feature-wrap">
                    <h2 class="sec-title t-center">Features of INGENERO’s Asset Performance Management Solution:</h2>

                    <div class="feature-card-wrap lblue-bg">
                        <div class="feature-card">
                          <div class="f-row f-2 f-1024-1">
                                <div class="f-col">
                                    <div class="feature-content">
                                        <h3 class="feature-title">Enhancing Exchanger Performance</h3>
                                        <div class="comm-para">
                                            <p>
                                                Exchangers play a crucial role in various industrial processes,
                                                facilitating heat exchange between fluids. However, issues such as
                                                fouling, corrosion, and inefficiencies can significantly impact their
                                                performance. INGENERO with its HMS-X soluton for exchangers employs
                                                advanced AI algorithms to continuously monitor exchanger operations,
                                                detecting anomalies and predicting potential failures well in advance.
                                                By analyzing real-time data streams, including temperature, pressure,
                                                and flow rates, their AI-based solutions enable proactive maintenance
                                                interventions, optimizing performance and extending the lifespan of
                                                exchangers.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="f-col">
                                    <div class="feature-img">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f1.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="feature-card-wrap l-yellow rev">
                        <div class="feature-card">
                          <div class="f-row f-2 f-1024-1">
                                <div class="f-col">
                                    <div class="feature-img">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f2.jpg" alt="">
                                    </div>
                                </div>
                                <div class="f-col">
                                    <div class="feature-content">
                                        <h3 class="feature-title">Optimizing Compressor Operations</h3>
                                        <div class="comm-para">
                                            <p>
                                                Compressors are vital components in industries ranging from oil and gas
                                                to manufacturing, responsible for maintaining optimal pressure levels in
                                                various processes. However, factors like wear and tear, fluctuating
                                                demand, and suboptimal operating conditions can compromise their
                                                reliability. INGENERO's with its HMC-C solution employs an AI-driven
                                                approach and leverages machine learning models trained on historical and
                                                real-time data to forecast compressor performance and identify
                                                inefficiencies. By monitoring key parameters such as vibration patterns,
                                                discharge pressure, and energy consumption, their solutions enable
                                                pre-emptive actions to mitigate potential issues, ensuring continuous
                                                operation and reducing maintenance costs.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="feature-card-wrap l-purple">
                        <div class="feature-card">
                          <div class="f-row f-2 f-1024-1">
                                <div class="f-col">
                                    <div class="feature-content">
                                        <h3 class="feature-title">Maximizing Furnace Efficiency</h3>
                                        <div class="comm-para">
                                            <p>
                                                INGENERO’s HMS-F solution employs latest ML/AI analytical techniques for
                                                enhancing furnace operations. Operations can monitor and track furnace
                                                health in real time and detect health/performance degradation such as
                                                coke formation during thermal cracking. HMS-F solution provides insights
                                                and intelligence for operators to take action to arrest further
                                                deterioration and
                                                extend furnace run length.
                                            </p>
                                            <p>The solution defines threshold limits for an furnace representing its end
                                                of run limit and provides notifications accordingly on time remaining,
                                                thereby improving scheduling, maintenance, and operations.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="f-col">
                                    <div class="feature-img">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/f3.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- images -->
        <section class="comm-section">
            <div class="container">
                <div class="img-box">
                    <div class="img-wrap">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/r1.jpg" alt="">
                    </div>
                    <div class="img-wrap">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/r2.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>

        <!-- use case -->
        <section class="comm-section">
            <div class="container">
                <div class="usecase-wrap">
                    <div class="section-heading">
                        <h2 class="sec-title t-center ">Use Cases</h2>
                    </div>

                    <div class="usecase-gallery">
                        <div class="usecase-card active">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                <h3 class="usecase-title">Oil and Gas</h3>

                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#oil">
                                            <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Oil and Gas</h3>
                            </div>
                        </div>
                        <div class="usecase-card">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <h3 class="usecase-title">Petrochemical</h3>
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#petro">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Petrochemical</h3>
                            </div>
                        </div>
                        <div class="usecase-card">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <h3 class="usecase-title">Refining</h3>
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#refine">
                                            <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Refining</h3>
                            </div>
                        </div>
                        <div class="usecase-card">
                            <div class="usecase-img">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt="">
                            </div>

                            <div class="usecase-content">
                                <div class="usecase-bottom">
                                    <h3 class="usecase-title">Power And Utilities</h3>
                                    <!-- <div class="comm-para">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.
                                        </p>
                                    </div> -->
                                    <div class="link-arrow">
                                        <a href="<?php echo get_site_url().'/knowledge-base'?>#power">
                                            <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="usecase-content-2">
                                <h3 class="usecase-title">Power And Utilities</h3>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

</div>
<!-- Main Container Ends -->


<?php get_footer(); ?>

<script>
        $(".usecase-card").hover(function () {
            $(".usecase-card").removeClass("active")
            $(this).addClass("active")
        })

        if (window.matchMedia("(max-width: 1024px)").matches) {
            $('.usecase-card').addClass('active')
        }

        /* digital capability sec script */
        var digitalSwiper = new Swiper(".digitalSwiper", {
            spaceBetween: 30,
            slidesPerView: 2.5,
            speed: 800,
            simulateTouch: false,
            allowTouchMove: false,

            scrollbar: {
                el: ".swiper-scrollbar",
            },

            breakpoints: {
                360: {
                    slidesPerView: 1,
                    allowTouchMove: true,
                },
                577: {
                    slidesPerView: 2,
                },
                1025: {
                    slidesPerView: 3,
                },
            },


        });

        let mm = gsap.matchMedia();

        mm.add("(min-width: 577px)", () => {

            const offerTxt = document.querySelectorAll(".trigger-point");
            offerTxt.forEach((section, i) => {
                const tlofferTxt = gsap.timeline({
                    scrollTrigger: {
                        trigger: section,
                        start: "0% 20%",
                        end: "0% 20%",
                        toggleActions: "play none none reverse",
                        onEnter: () => techSlideEnter(i),
                        onEnterBack: () => techSlideEnterBack(i),
                        // markers: true
                    },
                });
            });

            function techSlideEnter(i) {
                digitalSwiper.slideTo(i);
            }

            function techSlideEnterBack(i) {
                digitalSwiper.slideTo(i - 1);
            }

            var swiperTxtGroupCount = document.querySelectorAll(".swiperTxtGroup").length;

            $(".digitals-height-wrap").css({
                "height": (swiperTxtGroupCount * 80) + "vh"
            });

        })

        /* digital page glance sec script */
</script>