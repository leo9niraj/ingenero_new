<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package blank_theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class();  ?>>
    <?php wp_body_open(); ?>

    <header id="header" <?php   if (is_page_template('landing.php')) { ?> class="new-home-header" <?php } ?>>



        <div class="header-box">
            <div class="container">
                <div class="header-new-wrap">
                    <div class="header-top">
                        <div class="head-left">
                            <div class="logoBox">
                                <a class="logo" href="<?php echo get_site_url().'/'?>">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/logo.svg" alt="Logo" />
                                </a>
                            </div>
                        </div>
                        <div class="head-right">
                            <ul class="header-list">
                                <li class="drop">
                                    <a href="javascript:void(0)">About Us</a>
                                    <ul>
                                        <li><a href="<?php echo get_site_url().'/about'?>">About Us</a></li>
                                        <li><a href="<?php echo get_site_url().'/team'?>">Team</a></li>
                                    </ul>
                                </li>
                                <li class="drop">
                                    <a href="<?php echo get_site_url().'/sustainable'?>">Sustainability</a>
                                    <ul>
                                        <li><a href="<?php echo get_site_url().'/sustainable'?>#tab-1">Net Zero</a></li>
                                        <li><a href="<?php echo get_site_url().'/sustainable'?>#tab-2">Energy Management</a></li>
                                        <li><a href="<?php echo get_site_url().'/sustainable'?>#tab-4">Supply Chain Optimization</a></li>
                                        <!-- <li><a href="./our-uniqueness.html">C</a></li> -->
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo get_site_url().'/apply-ai'?>">Applied AI</a>
                                </li>
                                <li class="drop">
                                    <a href="<?php echo get_site_url().'/safety-service'?>">Safety</a>
                                    <ul>
                                        <li><a href="<?php echo get_site_url().'/safety-service'?>#safety">Safety Studies</a></li>
                                        <li><a href="<?php echo get_site_url().'/safety-service'?>#rel">Relief System Engineering</a></li>
                                        <li><a href="<?php echo get_site_url().'/safety-service'?>#securus">Codename Securus</a></li>
                                        <li><a href="<?php echo get_site_url().'/safety-service'?>#std">Software and Standards</a></li>
                                        <li><a href="<?php echo get_site_url().'/safety-service'?>#cover">Processes Covered</a></li>
                                        <!-- <li><a href="<?php echo get_site_url().'/safety-service'?>#webinar">Webinars</a></li> -->
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo get_site_url().'/reliability'?>">Reliability</a>
                                </li>
                                <li class="drop">
                                    <a href="<?php echo get_site_url().'/engineering-service'?>">Engineering</a>
                                    <ul>
                                        <li><a href="<?php echo get_site_url().'/engineering-service'?>#basic">Basic Engineering/FEED</a></li>
                                        <li><a href="<?php echo get_site_url().'/engineering-service'?>#detail">Detail Engineering</a></li>
                                        <li><a href="<?php echo get_site_url().'/engineering-service'?>#debottle-1">Debottlenecking</a></li>
                                        <li><a href="<?php echo get_site_url().'/engineering-service'?>#design">Process Design Package (PDP)</a>
                                        </li>
                                        <li><a href="<?php echo get_site_url().'/engineering-service'?>#concept">Concept Development/Process
                                                Development</a></li>
                                        <li><a href="<?php echo get_site_url().'/engineering-service'?>#debottle-4">Process Scale Up</a></li>
                                    </ul>
                                </li>


                                <li>
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>">Knowledge Base</a>
                                </li>


                            </ul>
                            <div class="menuBtn">
                                <a href="<?php echo get_site_url().'/contact'?>" class="button">Contact</a>
                                <div class="menu side-menu">
                                    <a href="javascript:void(0)" class="js-nav-toggle">
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="header-bottom">
                        <ul class="header-list">
                            <li class="drop">
                                <a href="#">Sustainability</a>
                                <ul>
                                    <li><a href="<?php echo get_site_url().'/sustainable'?>#netZero">Net Zero</a></li>
                                    <li><a href="<?php echo get_site_url().'/sustainable'?>#eneMgmt">Energy Management</a></li>
                                    <li><a href="<?php echo get_site_url().'/sustainable'?>#supply">Supply Chain Optimization</a></li>
                                </ul>
                            </li>
                            <li class="drop">
                                <a href="#">Digitalization</a>
                                <ul>
                                    <li><a href="<?php echo get_site_url().'/digitalization-service'?>#digiTwin">Digital Twin</a></li>
                                    <li><a href="<?php echo get_site_url().'/digitalization-service'?>#capab">Capabilities</a></li>
                                    <li><a href="<?php echo get_site_url().'/digitalization-service'?>#icap">Codename ICAP</a></li>
                                    <li><a href="<?php echo get_site_url().'/digitalization-service'?>#dashB">Dashboards</a></li>
                                    <li><a href="<?php echo get_site_url().'/digitalization-service'?>#roadMap">Roadmaps</a></li>
                                </ul>
                            </li>
                            <li class="drop">
                                <a href="<?php echo get_site_url().'/safety-service'?>">Safety</a>
                                <ul>
                                    <li><a href="<?php echo get_site_url().'/safety-service'?>#safety">Safety Studies</a></li>
                                    <li><a href="<?php echo get_site_url().'/safety-service'?>#rel">Relief System Engineering</a></li>
                                    <li><a href="<?php echo get_site_url().'/safety-service'?>#securus">Codename Securus</a></li>
                                    <li><a href="<?php echo get_site_url().'/safety-service'?>#std">Software and Standards</a></li>
                                    <li><a href="<?php echo get_site_url().'/safety-service'?>#cover">Processes Covered</a></li>
                                    <li><a href="<?php echo get_site_url().'/safety-service'?>#webinar">Webinars</a></li>
                                </ul>
                            </li>
                            <li class="drop">
                                <a href="<?php echo get_site_url().'/engineering-service'?>">Engineering</a>
                                <ul>
                                    <li><a href="<?php echo get_site_url().'/engineering-service'?>#basic">Basic Engineering/FEED</a></li>
                                    <li><a href="<?php echo get_site_url().'/engineering-service'?>#detail">Detail Engineering</a></li>
                                    <li><a href="<?php echo get_site_url().'/engineering-service'?>#debottle-1">Debottlenecking</a></li>
                                    <li><a href="<?php echo get_site_url().'/engineering-service'?>#design">Process Design Package (PDP)</a></li>
                                    <li><a href="<?php echo get_site_url().'/engineering-service'?>#concept">Concept Development/Process
                                            Development</a></li>
                                    <li><a href="<?php echo get_site_url().'/engineering-service'?>#debottle-4">Process Scale Up</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- Side Menu -->
        <!-- Side Menu -->
        <div class="menuOverlay"></div>
        <div class="nav-wrapper">
            <nav role="mob-navigation" class="mob-navigation">
                <div class="nav-toggle">
                    <span class="nav-back"></span>
                    <span class="nav-title">
                        Menu
                        <!-- <img src="./img/logo.svg" alt=""> -->
                    </span>
                    <span class="nav-close"></span>
                </div>

                <ul>
                    <li class="has-dropdown">
                        <a href="<?php echo get_site_url().'/about'?>">About Us</a>
                        <ul>
                            <li><a href="<?php echo get_site_url().'/about'?>">About Us</a></li>
                            <li><a href="<?php echo get_site_url().'/team'?>">Team</a></li>
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a href="#">Sustainability</a>
                        <ul>
                            <li><a href="<?php echo get_site_url().'/sustainable'?>#tab-1">Net Zero</a></li>
                            <li><a href="<?php echo get_site_url().'/sustainable'?>#tab-2">Energy Management</a></li>
                            <li><a href="<?php echo get_site_url().'/sustainable'?>#tab-4">Supply Chain Optimization</a></li>
                            <!-- <li><a href="./our-uniqueness.html">C</a></li> -->
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo get_site_url().'/apply-ai'?>">Applied AI</a>
                    </li>
                    <li class="has-dropdown">
                        <a href="<?php echo get_site_url().'/safety-service'?>">Safety</a>
                        <ul>
                            <li><a href="<?php echo get_site_url().'/safety-service'?>#safety">Safety Studies</a></li>
                            <li><a href="<?php echo get_site_url().'/safety-service'?>#rel">Relief System Engineering</a></li>
                            <li><a href="<?php echo get_site_url().'/safety-service'?>#securus">Codename Securus</a></li>
                            <li><a href="<?php echo get_site_url().'/safety-service'?>#std">Software and Standards</a></li>
                            <li><a href="<?php echo get_site_url().'/safety-service'?>#cover">Processes Covered</a></li>
                            <li><a href="<?php echo get_site_url().'/safety-service'?>#webinar">Webinars</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo get_site_url().'/reliability'?>">Reliability</a>
                    </li>
                    <li class="has-dropdown">
                        <a href="<?php echo get_site_url().'/engineering-service'?>">Engineering</a>
                        <ul>
                            <li><a href="<?php echo get_site_url().'/engineering-service'?>#basic">Basic Engineering/FEED</a></li>
                            <li><a href="<?php echo get_site_url().'/engineering-service'?>#detail">Detail Engineering</a></li>
                            <li><a href="<?php echo get_site_url().'/engineering-service'?>#debottle-1">Debottlenecking</a></li>
                            <li><a href="<?php echo get_site_url().'/engineering-service'?>#design">Process Design Package (PDP)</a></li>
                            <li><a href="<?php echo get_site_url().'/engineering-service'?>#concept">Concept Development/Process Development</a>
                            </li>
                            <li><a href="<?php echo get_site_url().'/engineering-service'?>#debottle-4">Process Scale Up</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo get_site_url().'/knowledge-base'?>">Knowledge Base</a>
                    </li>



                    <li>
                        <a href="<?php echo get_site_url().'/contact'?>">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>

    </header>
    <!-- #masthead -->