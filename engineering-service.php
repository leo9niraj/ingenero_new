<?php
/* Template Name: Engineering service */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); 

?>
<!-- Main Container Starts -->
<div class="main-container">

    <!-- banner section -->
    <?php $banner_section_data = get_field('banner_section');
                if ($banner_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="page-hdr">
                <div class="f-row">
                    <div class="w40 w-990-45 w-834-60 w-576-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title">Engineering <br />Services</h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p>Engineering</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-576-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p><?php echo $banner_section_data['sub_title'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-bnr">
                <img src="<?php echo $banner_section_data['banner_image'] ?>" alt="">
            </div>

            <div class="page-big-content">
                <h2 class="large-title"><?php echo $banner_section_data['content'] ?></h2>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- big text  -->
    <!-- <section class="">
    <div class="container">

    </div>
</section> -->

    <!-- consulting section -->
    <section class="comm-section">
        <div class="container">
            <?php $i = 1; if( have_rows('colour_full_cards') ): ?>
            <?php while( have_rows('colour_full_cards') ): the_row(); ?>

            <?php if($i == 1):?>
            <div class="learning-wrap lblue-bg img-text-box" id="basic">
                <div class="learning-content" >
                    <h2 class="large-title"><?php the_sub_field('title'); ?></h2>
                    <div class="comm-para">
                        <p><?php the_sub_field('content'); ?></p>
                    </div>
                </div>
                <div class="learning-img">
                    <img src="<?php the_sub_field('banner_image'); ?>" alt="">
                </div>
            </div>
            <?php endif; if($i == 2):?>
            <div class="learning-wrap l-yellow img-text-box img-left" id="detail">
                <div class="learning-img">
                    <img src="<?php the_sub_field('banner_image'); ?>" alt="">
                </div>
                <div class="learning-content">
                    <h2 class="large-title"><?php the_sub_field('title'); ?></h2>
                    <div class="comm-para">
                        <p><?php the_sub_field('content'); ?></p>
                    </div>
                </div>
            </div>
            <?php endif; if($i == 3):?>
            <div class="learning-wrap l-purple img-text-box" id="design">
                <div class="learning-content" >
                    <h2 class="large-title"><?php the_sub_field('title'); ?></h2>
                    <div class="comm-para">
                        <p><?php the_sub_field('content'); ?></p>
                    </div>
                </div>
                <div class="learning-img">
                    <img src="<?php the_sub_field('banner_image'); ?>" alt="">
                </div>
            </div>
            <?php endif;?>
            <?php $i++; endwhile; ?>
            <?php endif; ?>
        </div>
    </section>

    <!-- project section -->
    <section class="comm-section energy-sec" id="concept">
        <div class="container">
            <div class="project-wrap">

                <div class="project-height">
                    <div class="project-trigg project-1"></div>
                    <div class="project-trigg project-2"></div>
                    <div class="project-trigg project-3"></div>
                    <div class="project-trigg project-4"></div>
                    <div class="project-trigg project-5"></div>

                    <div class="project-sticky">
                        <h2 class="sec-title t-center">Engineering by Project Phase</h2>
                        <div class="project-box-wrap">
                            <div class="swiper projectLeftSwiper">
                                <div class="swiper-wrapper">
                                    
                                    <div class="swiper-slide">
                                        <div class="project-img l-orange project-tile img-1">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/project1.svg" alt="">
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="project-content project-tile">
                                            <h3 class="large-title">Decision Phase</h3>
                                            <div class="orange-arrow">
                                            <ul>
                                                <li>Process Design Package (PDP) </li>
                                                <li>Front End Engineering Design (FEED)</li>
                                                <li>Simulation &
                                                    Modeling</li>
                                            </ul>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="project-img l-orange project-tile img-3">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/project3.svg" alt="">
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="project-content project-tile">
                                            <h3 class="large-title">Commission & Start up</h3>
                                            <div class="orange-arrow">
                                            <ul>
                                                <li>Process Design Package (PDP) </li>
                                                <li>Front End Engineering Design (FEED)</li>
                                                <li>Simulation & Modeling</li>
                                            </ul>
                                        </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="project-timeline t1">
                                <div class="orange-timeline"></div>
                                <div class="phase-number project-tile">
                                    <div class="large-title white">
                                        <p>1</p>
                                    </div>
                                </div>
                            </div>


                            <div class="swiper projectRightSwiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="project-content project-tile" id="concept">
                                            <h3 class="large-title">Concept Phase</h3>
                                            <ul class="orange-arrow">
                                                <li>Process Design Package (PDP) </li>
                                                <li>Front End Engineering Design (FEED)</li>
                                                <li>Simulation &
                                                    Modeling</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="project-img l-orange project-tile img-2">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/project2.svg" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="project-content project-tile">
                                            <h3 class="large-title">Construction Phase</h3>
                                            <ul class="orange-arrow">
                                                <li>Process Design Package (PDP) </li>
                                                <li>Front End Engineering Design (FEED)</li>
                                                <li>Simulation &
                                                    Modeling</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="project-img l-orange project-tile img-4">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/project4.svg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mob-project-box">

                            <div class="project-card">
                                <div class="project-img l-orange project-tile img-1">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/project1.svg" alt="">
                                </div>
                                <div class="project-content project-tile">
                                    <h3 class="large-title">Concept Phase</h3>
                                    <ul class="orange-arrow">
                                        <li>Process Design Package (PDP) </li>
                                        <li>Front End Engineering Design (FEED)</li>
                                        <li>Simulation &
                                            Modeling</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="project-card">
                                <div class="project-img l-orange project-tile img-1">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/project2.svg" alt="">
                                </div>
                                <div class="project-content project-tile">
                                    <h3 class="large-title">Decision Phase</h3>
                                    <ul class="orange-arrow">
                                        <li>Process Design Package (PDP) </li>
                                        <li>Front End Engineering Design (FEED)</li>
                                        <li>Simulation &
                                            Modeling</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="project-card">
                                <div class="project-img l-orange project-tile img-1">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/project1.svg" alt="">
                                </div>
                                <div class="project-content project-tile">
                                    <h3 class="large-title">Construction Phase</h3>
                                    <ul class="orange-arrow">
                                        <li>Process Design Package (PDP) </li>
                                        <li>Front End Engineering Design (FEED)</li>
                                        <li>Simulation &
                                            Modeling</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="project-card">
                                <div class="project-img l-orange project-tile img-1">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/project4.svg" alt="">
                                </div>
                                <div class="project-content project-tile">
                                    <h3 class="large-title">Commission & Start up</h3>
                                    <ul class="orange-arrow">
                                        <li>Process Design Package (PDP) </li>
                                        <li>Front End Engineering Design (FEED)</li>
                                        <li>Simulation &
                                            Modeling</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Text section -->
    <section class="comm-section sticky-text-sec pt0">
        <div class="container">
            <?php $i = 1; 
            if( have_rows('conrten_slider') ): ?>
            <?php while( have_rows('conrten_slider') ): the_row(); ?>
            <div class="safety-study-content-wrap" id="debottle-<?php echo $i ?>">
                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left" >
                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold"><?php the_sub_field('title'); ?></h2>
                        </div>
                    </div>
                </div>
                <div class="safety-content-right">
                    <div class="relief-cont-wrap">
                        <div class="relief-cont">
                            <!-- <div class="comm-para">
                                <p>Ingenero offers debottlenecking services to the process industry, which is a
                                    critical strategy for optimizing production and efficiency. Our expertise
                                    involves identifying and eliminating constraints or limitations that hinder the
                                    smooth flow of a production system. We address these bottlenecks through various
                                    means, such as process redesign, equipment upgrades, or workflow improvements,
                                    helping our clients enhance productivity, reduce operational costs, and increase
                                    throughput.</p>

                            </div> -->
                            <?php the_sub_field('content'); ?>
                        </div>

                        <!-- <div class="relief-cont">
                            <div class="comm-para">
                                <p>Ingenero's debottlenecking solutions are pivotal in ensuring that existing
                                    facilities operate at their maximum capacity, making us a valuable partner for
                                    maintaining competitiveness and sustainability in the dynamic process industry.
                                </p>
                            </div>

                        </div> -->

                    </div>
                </div>

            </div>

            <?php $i++; endwhile; ?>
            <?php endif; ?>

            <!-- <div class="safety-study-content-wrap">

                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left">
                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold">Detailed Feasibility Reports (DFR)</h2>
                        </div>
                    </div>
                </div>

                <div class="safety-content-right">
                    <div class="relief-cont-wrap">
                        <div class="relief-cont">
                            <div class="med-para">
                                <p class="text-semiBold">Ingenero offers comprehensive expertise in chemical
                                    processes across various domains, from upstream to pharmaceuticals. This
                                    experience positions us as a trusted resource for developing Detailed
                                    Feasibility Reports (DFRs/FRs) and Conceptualizing processes (CDPs) when open
                                    art solutions suffice.</p>
                            </div>
                            <ul>
                                <li>Safe, cost-efficient THF solvent recovery process </li>
                                <li>Thorough utilities estimation
                                    including Class 4 cost assessment </li>
                                <li>Evaluation of lab data and experimental
                                    recommendations for fuel additives plants </li>
                                <li>Conversion of batch process to
                                    continuous for 2,5 dichlorophenol production</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="safety-study-content-wrap">

                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left" id="scale">

                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold">Process Development:</h2>
                        </div>
                    </div>
                </div>

                <div class="safety-content-right">
                    <div class="relief-cont-wrap">
                        <div class="relief-cont">
                            <div class="med-para">
                                <p class="text-semiBold">Ingenero specializes in process and product development
                                    while adhering to international safety and environmental standards. We handle
                                    various chemical reactions like:</p>
                            </div>
                            <div class="f-row f-2 f-480-1 two-col">
                                <div class="f-col">
                                    <ul>
                                        <li>Acylation </li>
                                        <li>Amidation</li>
                                        <li>Amination</li>
                                        <li>Condensation</li>
                                    </ul>
                                </div>
                                <div class="f-col">
                                    <ul>
                                        <li>Esterification </li>
                                        <li>Friedel-Crafts</li>
                                        <li>Hydrolysis</li>
                                        <li>Hydrogenation</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="med-para">
                                <p class="text-semiBold">Our services include</p>
                            </div>
                            <ul>
                                <li>Concept testing </li>
                                <li>Optimizing reaction conditions</li>
                                <li>Purifying products</li>
                                <li>Developing analytical methods</li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>

            <div class="safety-study-content-wrap">

                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left">

                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold">Process Scaleup</h2>
                        </div>
                    </div>
                </div>

                <div class="safety-content-right">
                    <div class="relief-cont-wrap">
                        <div class="relief-cont">
                            <div class="comm-para">
                                <p>To bring laboratory technology into industrial use, scaling up is necessary from
                                    small bench-scale (0.1 to 1 liter) to large industrial-scale (10,000 to
                                    1,000,000+ liters), which presents significant challenges and risks.
                                </p>
                            </div>
                        </div>

                        <div class="relief-cont">
                            <div class="comm-para">
                                <p>Scaling up from a pilot scale helps validate lab results, demonstrate
                                    scalability, address issues like sterility and heat transfer, integrate
                                    technology processes, and reduce commercialization risks.
                                </p>
                            </div>
                        </div>

                        <div class="relief-cont">
                            <div class="comm-para">
                                <p>Ingenero conducts pilot-scale demonstrations, offering third-party verification,
                                    scale-up assistance, design, construction, and operation services. We help our
                                    clients with flow sheet conceptualization, reactor and equipment design, cost
                                    estimation (equipment, fixed, and variable), and evaluate economic viability and
                                    minimum size requirements.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->


        </div>
    </section>

    <!-- use case -->
    <section class="comm-section">
        <div class="container">
            <div class="usecase-wrap">
                <div class="section-heading">
                    <h2 class="sec-title t-center ">Use Cases</h2>
                </div>

                <div class="usecase-gallery">
                    <div class="usecase-card active">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                            <h3 class="usecase-title">Oil and Gas</h3>

                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#oil">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Oil and Gas</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Petrochemical</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#petro">
                                    <!-- <i class="ph-bold ph-arrow-right"></i> -->
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Petrochemical</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Refining</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#refine">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Refining</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Power And Utilities</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#power">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Power And Utilities</h3>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    
    <?php $pre_footer_section_data = get_field('pre_footer_section');
                if ($pre_footer_section_data) : ?>
    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white"><?php echo $pre_footer_section_data['title'] ?></h2>
                <div class="comm-para t-center white">
                    <p><?php echo $pre_footer_section_data['sub_title'] ?></p>
                </div>
                <a href="<?php echo $pre_footer_section_data['cta'] ?>" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>
<!-- Main Container Ends -->


<?php get_footer(); ?>

<script>
        $(".usecase-card").hover(function () {
            $(".usecase-card").removeClass("active")
            $(this).addClass("active")
        })

        if (window.matchMedia("(max-width: 1024px)").matches) {
            $('.usecase-card').addClass('active')
        }

        /* digital capability sec script */
        var digitalSwiper = new Swiper(".digitalSwiper", {
            spaceBetween: 30,
            slidesPerView: 2.5,
            speed: 800,
            simulateTouch: false,
            allowTouchMove: false,

            scrollbar: {
                el: ".swiper-scrollbar",
            },

            breakpoints: {
                360: {
                    slidesPerView: 1,
                    allowTouchMove: true,
                },
                577: {
                    slidesPerView: 2,
                },
                1025: {
                    slidesPerView: 3,
                },
            },


        });

        let mm = gsap.matchMedia();

        mm.add("(min-width: 577px)", () => {

            const offerTxt = document.querySelectorAll(".trigger-point");
            offerTxt.forEach((section, i) => {
                const tlofferTxt = gsap.timeline({
                    scrollTrigger: {
                        trigger: section,
                        start: "0% 20%",
                        end: "0% 20%",
                        toggleActions: "play none none reverse",
                        onEnter: () => techSlideEnter(i),
                        onEnterBack: () => techSlideEnterBack(i),
                        // markers: true
                    },
                });
            });

            function techSlideEnter(i) {
                digitalSwiper.slideTo(i);
            }

            function techSlideEnterBack(i) {
                digitalSwiper.slideTo(i - 1);
            }

            var swiperTxtGroupCount = document.querySelectorAll(".swiperTxtGroup").length;

            $(".digitals-height-wrap").css({
                "height": (swiperTxtGroupCount * 80) + "vh"
            });

        })

        /* digital page glance sec script */
</script>