<?php
/* Template Name: About */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>



<!-- Main Container Starts -->
<div class="main-container">
    <section class="comm-section">
        <div class="container">
            <div class="page-hdr">
                <div class="f-row">
                    <div class="w30 w-990-45 w-834-60 w-576-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title">About Us</h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p>About Us</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-576-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p><?php echo get_field('title')?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $banner_section_data = get_field('banner_section');
            if ($banner_section_data) : ?>
            <!-- <div class="page-bnr">
                <img src="<?php echo $banner_section_data['banner_image'] ?>" alt="">
            </div> -->
            <div class="page-big-content">
                <h2 class="large-title"><?php echo $banner_section_data['content'] ?></h2>
            </div>
            <?php endif; ?>
        </div>
    </section>


    <!-- visionmission sec  -->
    <section class="comm-section vision-sec">
        <div class="container">
            <div class="f-row">
            <?php $vision_section_data = get_field('vision_section');
            if ($vision_section_data) : ?>
                <div class="w50 w-1064-100">
                    <div class="l-orange vision-mission-box">
                        <div class="vm-icon">
                            <img src="<?php echo $vision_section_data['icon'] ?>" alt="">
                        </div>
                        <div class="vm-content">
                            <h2 class="sec-title"><?php echo $vision_section_data['title'] ?> </h2>
                            <div class="med-para">
                                <p><?php echo $vision_section_data['content'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>


                <?php $mission_section_data = get_field('mission_section');
                if ($mission_section_data) : ?>
                <div class="w50 w-1064-100">
                    <div class="l-yellow vision-mission-box">
                        <div class="vm-icon">
                            <img src="<?php echo $mission_section_data['icon'] ?>" alt="">
                        </div>
                        <div class="vm-content">
                            <h2 class="sec-title"><?php echo $mission_section_data['title'] ?> </h2>
                            <ul class="mission-points">
                                <!-- <li>
                                    <p>To ensure delivery of consistent high-value and high-quality services to our
                                        customers
                                    </p>
                                </li>
                                <li>
                                    <p>To create a nurturing environment for engineers at Ingenero, fostering
                                        personal and professional growth opportunities and serving as a sanctuary
                                        where they can thrive.</p>
                                </li> -->
                                <?php echo $mission_section_data['content'] ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <!-- value sec -->
    <section class="comm-section">
        <div class="container">
            <div class="lblue-bg value-box">
                <h2 class="sec-title t-center">Our Values</h2>
                <div class="value-content-wrap">
                    <div class="f-row">
                        <?php if( have_rows('values_section') ): ?>
                        <?php while( have_rows('values_section') ): the_row();  ?>
                        <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php the_sub_field('icon'); ?>" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title"><?php the_sub_field('title'); ?></h3>
                                    <div class="comm-para">
                                        <p><?php the_sub_field('content'); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <!-- <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/valie-icon-2.svg" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title">Partnership</h3>
                                    <div class="comm-para">
                                        <p>To gather more ideas that when put together can build a sustainable and
                                            profitable business.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/valie-icon-3.svg" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title">Ownership</h3>
                                    <div class="comm-para">
                                        <p>Take the initiative to bring about positive results and be accountable
                                            for the results of your actions, ensuring they are of the highest
                                            quality and delivered in a timely manner.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/valie-icon-4.svg" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title">Global Perspective</h3>
                                    <div class="comm-para">
                                        <p>Be open to new ideas, issues, and solutions, and to changing the way we
                                            do things if we find a new system that works better.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/valie-icon-5.svg" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title">Dependability</h3>
                                    <div class="comm-para">
                                        <p>We are committed to saying what we will do and doing what we say.
                                            Furthermore, we are constantly refining our skills to ensure that we
                                            remain the rock for our customers' needs.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/valie-icon-6.svg" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title">Empowerment</h3>
                                    <div class="comm-para">
                                        <p>Encourage to transform choices into desired outcomes & actions</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/valie-icon-7.svg" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title">Enterprising</h3>
                                    <div class="comm-para">
                                        <p>Be enterprising, always ready to embark on new ventures. Use initiative
                                            and energy to try out unusual ways of achieving goals, while also
                                            employing judgement to take calculated risks and seize opportunities as
                                            they arise.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w45 w-1280-50 w-1064-100">
                            <div class="value-content">
                                <div class="value-icon">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/valie-icon-8.svg" alt="">
                                </div>
                                <div class="value-detail">
                                    <h3 class="small-title">Passion</h3>
                                    <div class="comm-para">
                                        <p>Have a passion for technology to keep continuously learning the newest
                                            advancements. Drive to elevate service offerings to clients while
                                            maintaining high standards</p>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- quality sec -->
    <?php $quality_section_data = get_field('quality_section');
                if ($quality_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="quality-sec-wrap">
                <div class="quality-left">
                    <h2 class="sec-title"><?php echo $quality_section_data['title'] ?></h2>
                    <div class="med-para">
                        <p class="text-medium"><?php echo $quality_section_data['content'] ?></p>
                    </div>
                </div>
                <div class="quality-right">
                    <div class="quality-img">
                        <img src="<?php echo $quality_section_data['image'] ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>


    <!-- culture content sec -->

    <?php $culture_section_data = get_field('culture_section');
                if ($culture_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="culture-text-hdr">
                <h2 class="sec-title "><?php echo $culture_section_data['title'] ?></h2>
                <h3 class="large-title"><?php echo $culture_section_data['sub_title'] ?></h3>
                <div class="med-para">
                    <p><?php echo $culture_section_data['content'] ?></p>
                </div>
                <a href="<?php echo $culture_section_data['read_more'] ?>" class="button read-btn">Read More</a>
            </div>
        </div>
    </section>
   
    <!-- culture swiper sec -->
    <section class="comm-section">
        <div class="container">
            <div class="swiper cultureSwiper culture-swiper">
                <div class="swiper-wrapper">
                <?php if (have_rows('culture_section')) : while (have_rows('culture_section')) : the_row();
                    if (have_rows('images_slider')) : while (have_rows('images_slider')) : the_row(); ?>
                    <div class="swiper-slide">
                        <div class="culture-img">
                            <img src="<?php the_sub_field('image')?>" alt="">
                        </div>
                    </div>
                    <?php endwhile;endif;
                endwhile;endif?>
                    <!-- <div class="swiper-slide">
                        <div class="culture-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/culture-img-2.jpg" alt="">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="culture-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/culture-img-1.jpg" alt="">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="culture-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/culture-img-2.jpg" alt="">
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- secure policy sec -->
    <?php $security_section_data = get_field('security_section');
    if ($security_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="quality-sec-wrap">
                <div class="f-row">
                    <div class="w35 w-1064-30 w-990-100">
                        <h2 class="sec-title"><?php echo $security_section_data['title'] ?></h2>
                        <div class="med-para">
                            <p><?php echo $security_section_data['sub_title'] ?></p>
                        </div>
                    </div>

                    <div class="w50 w-1064-65 w-990-100">

                        <div class="security-right">
                            <div class="f-row f-2 f-640-1">

                            <?php if (have_rows('security_section')) : while (have_rows('security_section')) : the_row();
                                    if (have_rows('security_cards')) : while (have_rows('security_cards')) : the_row(); ?>
                                <div class="f-col">
                                    <div class="l-orange securiry-box">
                                        <div class="security-icon">
                                            <img src="<?php the_sub_field('icon')?>"
                                                alt="">
                                        </div>
                                        <div class="security-text">
                                            <p><?php the_sub_field('title')?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile;endif;
                                    endwhile;endif?>

                                <!-- <div class="f-col">
                                    <div class="l-orange securiry-box">
                                        <div class="security-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/secure-icon-2.svg"
                                                alt="">
                                        </div>
                                        <div class="security-text">
                                            <p>Protect personal information in all its form</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="f-col">
                                    <div class="l-orange securiry-box">
                                        <div class="security-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/secure-icon-3.svg"
                                                alt="">
                                        </div>
                                        <div class="security-text">
                                            <p>Confidentiality of information is assured.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="f-col">
                                    <div class="l-orange securiry-box">
                                        <div class="security-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/secure-icon-4.svg"
                                                alt="">
                                        </div>
                                        <div class="security-text">
                                            <p>Legislative and regulatory requirements are complied with.</p>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <?php $pre_footer_section_data = get_field('pre_footer_section');
                if ($pre_footer_section_data) : ?>
    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white"><?php echo $pre_footer_section_data['title'] ?></h2>
                <div class="comm-para t-center white">
                    <p><?php echo $pre_footer_section_data['sub_title'] ?></p>
                </div>
                <a href="<?php echo $pre_footer_section_data['cta'] ?>" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>
<!-- Main Container Ends -->


<?php get_footer(); ?>

<script>
var cultureSwiper = new Swiper(".cultureSwiper", {
    slidesPerView: 1.7,
    spaceBetween: 40,
    autoplay: {
        delay: 2000,
        disableOnInteraction: false
    }
});
</script>