<?php
/* Template Name: Team */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>


<!-- Main Container Starts -->
<div class="main-container">

    <section class="comm-section pb0">
        <div class="container">
            <div class="page-hdr team-hdr">
                <div class="f-row">
                    <div class="w40 w-1064-60 w-834-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title"><?php echo get_field('title') ?></h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p>Engineering</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-834-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p><?php echo get_field('sub_title') ?> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $team_data = get_field('team');
            if ($team_data) : ?>

            <!-- <div class="team-header-wrap">
                <div class="section-heading">
                    <h2 class="sec-title t-center"><?php echo $team_data['title'] ?></h2>
                </div>
                <div class="med-para">
                    <p class="t-center"><?php echo $team_data['sub_title'] ?></p>
                </div>
            </div> -->
        </div>
    </section>
   

    <!-- more team boxes sec -->
    <section class="comm-section top-space">
        <div class="container">
            <div class="team-mem-boxes-wrap">
                <div class="f-row f-3 f-1200-2 f-640-1">
                <?php $i = 1; $x = 0;
                    if (have_rows('team')) : while (have_rows('team')) : the_row();
                         if (have_rows('team_cards')) : while (have_rows('team_cards')) : the_row(); ?>
                    <div class="f-col">
                        <div class="team-box">
                            <div class="team-img"><img src="<?php the_sub_field('banner_image'); ?>" alt=""></div>
                            <div class="team-box-btm">
                                <div class="team-prof-dtl">
                                    <h3><?php the_sub_field('name'); ?></h3>
                                    <div class="team-desig comm-para">
                                        <p><?php the_sub_field('designation'); ?></p>
                                    </div>
                                </div>
                                <div class="plus-btn plus-icon">+</div>
                            </div>
                            <div class="team-detail">
                                <h4><?php the_sub_field('name'); ?></h4>
                                <div class="comm-para">
                                    <p><?php the_sub_field('designation'); ?></p>
                                </div>
                                <!-- <ul class="team-dtl-content">
                                    <li> MD, Kilimanjaro Partners</li>
                                    <li>Founder, Acorn Systems </li>
                                    <li>Strategy Consultant, Booz Allen & Hamilton </li>
                                    <li>Sr. Process Engineer / Environmental Mgr, Solvay </li>
                                    <li>M.Ch.E., B.S., Rice University </li>
                                    <li>MBA, Harvard </li>
                                </ul> -->
                                <?php the_sub_field('about'); ?>
                            </div>
                        </div>
                    </div>
                    <?php $i++; 
                            endwhile;endif;
                    endwhile;endif?>
                    <!-- <div class="f-col">
                        <div class="team-box">
                            <div class="team-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/team-img-2.jpg" alt=""></div>
                            <div class="team-box-btm">
                                <div class="team-prof-dtl">
                                    <h3>Pratap Nair</h3>
                                    <div class="team-desig comm-para">
                                        <p>President & CEO, Founder</p>
                                    </div>
                                </div>
                                <div class="plus-btn plus-icon">+</div>
                            </div>
                            <div class="team-detail">
                                <h4>Pratap Nair</h4>
                                <div class="comm-para">
                                    <p>President & CEO, Founder</p>
                                </div>
                                <ul class="team-dtl-content">
                                    <li> MD, Kilimanjaro Partners</li>
                                    <li>Founder, Acorn Systems </li>
                                    <li>Strategy Consultant, Booz Allen & Hamilton </li>
                                    <li>Sr. Process Engineer / Environmental Mgr, Solvay </li>
                                    <li>M.Ch.E., B.S., Rice University </li>
                                    <li>MBA, Harvard </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="f-col">
                        <div class="team-box">
                            <div class="team-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/team-img-3.jpg" alt=""></div>
                            <div class="team-box-btm">
                                <div class="team-prof-dtl">
                                    <h3>Kishor Patil</h3>
                                    <div class="team-desig comm-para">
                                        <p>COO, Founder</p>
                                    </div>
                                </div>
                                <div class="plus-btn plus-icon">+</div>
                            </div>
                            <div class="team-detail">
                                <h4>Pratap Nair</h4>
                                <div class="comm-para">
                                    <p>President & CEO, Founder</p>
                                </div>
                                <ul class="team-dtl-content">
                                    <li> MD, Kilimanjaro Partners</li>
                                    <li>Founder, Acorn Systems </li>
                                    <li>Strategy Consultant, Booz Allen & Hamilton </li>
                                    <li>Sr. Process Engineer / Environmental Mgr, Solvay </li>
                                    <li>M.Ch.E., B.S., Rice University </li>
                                    <li>MBA, Harvard </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="f-col">
                        <div class="team-box">
                            <div class="team-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/team-img-4.jpg" alt=""></div>
                            <div class="team-box-btm">
                                <div class="team-prof-dtl">
                                    <h3>Jim Brigman</h3>
                                    <div class="team-desig comm-para">
                                        <p>Managing Director, Principal</p>
                                    </div>
                                </div>
                                <div class="plus-btn plus-icon">+</div>
                            </div>
                            <div class="team-detail">
                                <h4>Pratap Nair</h4>
                                <div class="comm-para">
                                    <p>President & CEO, Founder</p>
                                </div>
                                <ul class="team-dtl-content">
                                    <li> MD, Kilimanjaro Partners</li>
                                    <li>Founder, Acorn Systems </li>
                                    <li>Strategy Consultant, Booz Allen & Hamilton </li>
                                    <li>Sr. Process Engineer / Environmental Mgr, Solvay </li>
                                    <li>M.Ch.E., B.S., Rice University </li>
                                    <li>MBA, Harvard </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="f-col">
                        <div class="team-box">
                            <div class="team-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/team-img-5.jpg" alt=""></div>
                            <div class="team-box-btm">
                                <div class="team-prof-dtl">
                                    <h3>Pratap Nair</h3>
                                    <div class="team-desig comm-para">
                                        <p>President & CEO, Founder</p>
                                    </div>
                                </div>
                                <div class="plus-btn plus-icon">+</div>
                            </div>
                            <div class="team-detail">
                                <h4>Pratap Nair</h4>
                                <div class="comm-para">
                                    <p>President & CEO, Founder</p>
                                </div>
                                <ul class="team-dtl-content">
                                    <li> MD, Kilimanjaro Partners</li>
                                    <li>Founder, Acorn Systems </li>
                                    <li>Strategy Consultant, Booz Allen & Hamilton </li>
                                    <li>Sr. Process Engineer / Environmental Mgr, Solvay </li>
                                    <li>M.Ch.E., B.S., Rice University </li>
                                    <li>MBA, Harvard </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="f-col">
                        <div class="team-box">
                            <div class="team-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/team-img-6.jpg" alt=""></div>
                            <div class="team-box-btm">
                                <div class="team-prof-dtl">
                                    <h3>Kishor Patil</h3>
                                    <div class="team-desig comm-para">
                                        <p>COO, Founder</p>
                                    </div>
                                </div>
                                <div class="plus-btn plus-icon">+</div>
                            </div>
                            <div class="team-detail">
                                <h4>Pratap Nair</h4>
                                <div class="comm-para">
                                    <p>President & CEO, Founder</p>
                                </div>
                                <ul class="team-dtl-content">
                                    <li> MD, Kilimanjaro Partners</li>
                                    <li>Founder, Acorn Systems </li>
                                    <li>Strategy Consultant, Booz Allen & Hamilton </li>
                                    <li>Sr. Process Engineer / Environmental Mgr, Solvay </li>
                                    <li>M.Ch.E., B.S., Rice University </li>
                                    <li>MBA, Harvard </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- learning section -->
    <?php $advisors_data = get_field('advisors');
            if ($advisors_data) : ?>
    <!-- <section class="comm-section">
        <div class="container">
            <div class="team-header-wrap">
                <div class="section-heading">
                    <h2 class="sec-title t-center"><?php echo $advisors_data['title'] ?></h2>
                </div>
            </div>

            <div class="team-swiper-wrap">
                <div class="advisor-wrap lblue-bg">

                    <div class="swiper teamImgSwiper team-img-swiper">
                        <div class="swiper-wrapper">
                        <?php $i = 1; $x = 0;
                            if (have_rows('advisors')) : while (have_rows('advisors')) : the_row();
                         if (have_rows('advisors_cards')) : while (have_rows('advisors_cards')) : the_row(); ?>
                            <div class="swiper-slide">
                                <div class="advisor-img">
                                    <img src="<?php the_sub_field('banner_image'); ?>" alt="" width="100%" height="100%" />
                                </div>
                            </div>
                            <?php endwhile;endif;
                            endwhile;endif?>
                           
                        </div>
                    </div>

                    <div class="swiper teamInfoSwiper team-info-swiper">
                        <div class="swiper-wrapper">
                        <?php $i = 1; $x = 0;
                    if (have_rows('advisors')) : while (have_rows('advisors')) : the_row();
                         if (have_rows('advisors_cards')) : while (have_rows('advisors_cards')) : the_row(); ?>
                            <div class="swiper-slide">
                                <div class="advisor-content">
                                    <h2 class="small-title"><?php the_sub_field('name'); ?></h2>
                                    <div class="comm-para">
                                        <p><?php the_sub_field('about'); ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile;endif;
                endwhile;endif?>
                            

                        </div>

                        <div class="team-nav-wrap">
                            <div class="team-nav team-prev">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/right-arrow.svg" alt="">
                            </div>
                            <div class="team-nav team-next">
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/right-arrow.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section> -->
    <?php endif; ?>


    <?php $pre_footer_section_data = get_field('pre_footer_section');
                if ($pre_footer_section_data) : ?>
    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white"><?php echo $pre_footer_section_data['title'] ?></h2>
                <div class="comm-para t-center white">
                    <p><?php echo $pre_footer_section_data['sub_title'] ?></p>
                </div>
                <a href="<?php echo $pre_footer_section_data['cta'] ?>" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>
<!-- Main Container Ends -->

<?php get_footer(); ?>