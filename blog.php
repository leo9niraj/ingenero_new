<?php
/* Template Name: Blog */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>

<!-- Main Container Starts -->
<div class="main-container">

    <section class="comm-section">
        <div class="container">
            <div class="page-hdr team-hdr">
                <div class="f-row">
                    <div class="w40 w-1064-60 w-834-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title">Blog</h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p>Blog</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-834-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p>Learn more about Ingenero's successes and critical industrial insights</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="blog-wrap">
                <div class="f-row f-3 f-990-2 f-576-1">

                    <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $queryObject = new  Wp_Query(array(
                        'posts_per_page'   => '-1',
                        'post_type'        => 'post',
                        'post_status'      => 'publish',
                        'paged' => $paged,

                    )); ?>
                    <?php
                $cnt = 1;
                if ($queryObject->have_posts()) {
                    while ($queryObject->have_posts()) : $queryObject->the_post();
                        $title = get_the_title();
                ?>
                    <a href="<?php the_permalink() ?>" class="f-col">
                        <div class="cs-swiper-box">
                            <div>
                                <div class="cs-swiper-img">
                                    <?php  $url =  get_the_post_thumbnail_url();  ?>
                                    <img src="<?php echo esc_url( $url ); ?>"
                                        alt="<?php echo get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>"
                                        width="100%" height="100%" loading="eager">
                                </div>

                                <div class="cs-swiper-box-btm">
                                    <h3 class="knowledge-card-title"><?php the_title() ?></h3>
                                </div>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <?php  $categories = $categories = get_the_category($post_id);
                                        foreach($categories as $category) { ?>
                                    <span><?php echo $category->name ?></span>
                                    <?php } ?>
                                </div>
                                <span><?php echo get_the_date() ?></span>
                            </div>
                        </div>
                    </a>
                    <?php $cnt++;
                endwhile;
                }
                wp_reset_query();  // Restore global post data stomped by the_post().
                ?>

                    <!-- <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-2.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div> -->


                    <!-- <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-3.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates
                                    20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-1.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-2.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-3.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates
                                    20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-1.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-2.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-3.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates
                                    20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-1.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-2.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-3.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates
                                    20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-1.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-2.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates 20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div>
                    <div class="f-col">
                        <div class="cs-swiper-box">
                            <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/cs-img-3.jpg" alt=""></div>
                            <div class="cs-swiper-box-btm">
                                <h3 class="knowledge-card-title">Furnace Convection Section Model facilitates
                                    20%
                                    rise in Plant Capacity</h3>
                            </div>
                            <div class="blog-detail">
                                <div class="chip">
                                    <span>Category</span>
                                </div>
                                <span>25 Apr. 2023</span>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <a href="<?php echo get_site_url().'/blog/'?>" class="button line mob-btn">View All</a>
        </div>
    </section>


    <?php $pre_footer_section_data = get_field('pre_footer_section');
                if ($pre_footer_section_data) : ?>
    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white"><?php echo $pre_footer_section_data['title'] ?></h2>
                <div class="comm-para t-center white">
                    <p><?php echo $pre_footer_section_data['sub_title'] ?></p>
                </div>
                <a href="<?php echo $pre_footer_section_data['cta'] ?>" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
<!-- Main Container Ends -->




<?php get_footer(); ?>