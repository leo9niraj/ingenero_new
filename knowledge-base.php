<?php
/* Template Name: knowledge base */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>


<!-- Main Container Starts -->
<div class="main-container">

    <section class="comm-section">
        <div class="container">

            <div class="page-hdr team-hdr">
                <div class="f-row">
                    <div class="w40 w-1064-60 w-834-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title">Knowledge Base</h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="">Home</a>
                                    </li>
                                    <li>
                                        <p>Knowledge Base</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-834-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="blog-wrap knowledge-wrap">

                <!-- oil -->
                <div class="category-wrap">
                    <h2 class="blog-category" id="oil">Sample List of Reference Projects</h2>
                    <div class="f-row f-4 f-1280-3 f-990-2 f-576-1">
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Sampe list of Projects.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u5.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Sampe list of Projects </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- oil -->
                <div class="category-wrap">
                    <h2 class="blog-category" id="oil">Oil and Gas</h2>
                    <div class="f-row f-4 f-1280-3 f-990-2 f-576-1">
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Oil & Gas/Oil & Gas Case Use Case.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Oil & Gas Use case 1 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- Petrochemical -->
                <div class="category-wrap">
                    <h2 class="blog-category" id="petro">Petrochemical</h2>
                    <div class="f-row f-4 f-1280-3 f-990-2 f-576-1">
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case1.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 1 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case2.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 2 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case3.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 3 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case4.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 4 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case5.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 5 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case6.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 6 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case7.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 7 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case8.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 8 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case9.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 9 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case10.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 10 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Petrochemicals/Petrochemicals Use Case11.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Petrochemicals Use case 11 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- refine -->
                <div class="category-wrap">
                    <h2 class="blog-category" id="refine">Refining</h2>
                    <div class="f-row f-4 f-1280-3 f-990-2 f-576-1">
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Refining/Refining Use Case1.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Refining Use case 1 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Refining/Refining Use Case2.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Refining Use case 2 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Refining/Refining Use Case3.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Refining Use case 3 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Refining/Refining Use Case4.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Refining Use case 4 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Refining/Refining Use Case5.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Refining Use case 5 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Refining/Refining Use Case6.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Refining Use case 6 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- power -->
                <div class="category-wrap">
                    <h2 class="blog-category" id="power">Power And Utilities</h2>
                    <div class="f-row f-4 f-1280-3 f-990-2 f-576-1">
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Power & Utilities/Power & Utilities Case Study1.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Use case 1 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="f-col">
                            <a href="<?php bloginfo('template_url'); ?>/assets/pdf/Power & Utilities/Power & Utilities Case Study2.pdf" download>
                                <div class="cs-swiper-box">
                                    <div>
                                        <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt=""></div>
                                        <div class="cs-swiper-box-btm">
                                            <h3 class="knowledge-card-title">Power & Utilities Use case 2 </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>

    <!-- <a href="" class="button line mob-btn">View All</a> -->




</div>
<!-- Main Container Ends -->


<?php get_footer(); ?>