// /Counter Script/
// var a = 0;
// $(window).scroll(function () {
//     winHeight = window.innerHeight;
//     var oTop = $(".serv-sec").offset().top - winHeight * 0.5;
//     if (a == 0 && $(window).scrollTop() > oTop) {
//         $(".counterValue").each(function () {
//             var $this = $(this),
//                 countTo = $this.attr("data-count");
//             $({
//                 countNum: $this.text(),
//             }).animate(
//                 {
//                     countNum: countTo,
//                 },
//                 {
//                     duration: 1200,
//                     easing: "swing",
//                     step: function () {
//                         $this.text(Math.floor(this.countNum));
//                     },
//                     complete: function () {
//                         $this.text(this.countNum);
//                     },
//                 }
//             );
//         });
//         a = 1;
//     }
// });

// Anim text starts
window.addEventListener("DOMContentLoaded", () => {
    gsap.config({ trialWarn: false });
    // console.clear();
    gsap.registerPlugin(ScrollTrigger, SplitText);
    let split = new SplitText(".anim-text", { type: "lines" });

    function makeItHappen() {
        split.lines.forEach((target) => {
            gsap.to(target, {
                backgroundPositionX: 0,
                ease: "none",
                scrollTrigger: {
                    trigger: ".show-orange-sec-trigger",
                    markers: true,
                    scrub: 0.5,
                    start: "0% 50%",
                    end: "0% 0%",
                },
                opacity: 1,
            });
        });
    }

    function makeItHappenMobile() {
        split.lines.forEach((target) => {
            gsap.to(target, {
                backgroundPositionX: 0,
                ease: "none",
                scrollTrigger: {
                    trigger: target,
                    // markers: true,
                    scrub: 0.5,
                    start: "top 50%",
                    end: "bottom 50%",
                },
                opacity: 1,
            });
        });
    }

    // makeItHappen();

    makeItHappenMobile();

});

$(document).ready(function () {

    // banner youtube popup
    $(document).ready(function () {
        $('.comm-yt-popup').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: true,
            fixedContentPos: false
        });
    });

    // adding class for different header in homepage
    // $('body').addClass('home-header')

    // show first content by default
    // $('#tabs-nav li:first-child').addClass('active');
    // $('.content').hide();
    // $('.content:first').show();

    // // click function
    // $('#tabs-nav li').click(function () {
    //     $('#tabs-nav li').removeClass('active');
    //     $(this).addClass('active');
    //     $('.content').hide();
    //     var activeTab = $(this).find('a').attr('href');
    //     $(activeTab).fadeIn();
    //     return false;
    // });

    // industry detail swiper
    var indDetailSwiper = new Swiper(".indDetailSwiper", {
        direction: "vertical",
        effect: "fade",
        loop: true,
        allowTouchMove: false,
    });
    // industry name swiper
    var indNameSwiper = new Swiper(".indNameSwiper", {
        direction: "vertical",
        slidesPerView: 3,
        spaceBetween: 0,
        loop: true,
        centeredSlides: true,
        // infinite: true,
        // autoplay: {
        //     delay: 0,
        //     disableOnInteraction: false
        // },
        // slidesPerView: 'auto',
        // spaceBetween: 10,
        speed: 5000,
        // loop: true,
        // simulateTouch: false,
        autoplay: {
            delay: 0,
            disableOnInteraction: true,
            // reverseDirection: reverse,
        },
        grabCursor: false,
        // slidesPerView: 'auto',
        // freeMode: true,
        // speed: 3000,
        on: {
            slideChange: function () {
                i = this.realIndex;
                // console.log(i);
                indDetailSwiper.slideTo(i + 1);
            },
        }
    });

    // /* casestudy slider */
    var caseSwiper = new Swiper(".caseSwiper", {
        slidesPerView: 3,
        spaceBetween: 30,

        breakpoints: {
            320: {
                slidesPerView: 1.2,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            1366: {
                slidesPerView: 3,
                spaceBetween: 40
            }
        }
    });

    gsap.registerPlugin(ScrollTrigger);
    gsap.defaults({
        ease: "power2.inOut",
        duration: 1,
    });

    let mm = gsap.matchMedia();

    // banner triggers
    const bannerTriggers = document.querySelectorAll(".banner-trigg");

    // dynamic height
    mm.add("(min-width: 1025px)", () => {
        $('.banner-height').css('height', (bannerTriggers.length * 100) + 'vh')

        var topPadding = $('.main-container').css('padding-top')

        bannerTriggers.forEach((section, i) => {

            // trigger point dynamic css
            $(".banner-" + (i + 1)).css({
                top: i * 100 + "vh",
                height: 100 + "vh",
            });

            // banner section pinning
            var projectPinTl = gsap.timeline({
                scrollTrigger: {
                    trigger: ".banner-height",
                    start: `0% ${topPadding}`,
                    end: "100% -100%",
                    pin: ".banner-sticky",
                    // pin: ".t1",
                    scrub: 1,
                    // markers: true,
                    id: "pin",
                },
            });

            // banner scroll anim
            var bannerScrollTl = gsap.timeline({
                scrollTrigger: {
                    trigger: section,
                    start: "0% 50%",
                    end: "100% 50%",
                    // toggleActions: "play none none reverse",
                    scrub: true,
                    // markers: true,
                    id: "scroll" + i,
                },
            })

            function onloadAnim() {
                gsap.to('.home-banner-video',
                    {
                        duration: 2,
                        autoAlpha: 1,
                    }, 0)
                gsap.from("#header",
                    {
                        y: '-100%',
                        duration: 1.5
                    }, 0.5)
            }

            function firstAnim() {
                bannerScrollTl
                    .to('.home-banner-video',
                        {
                            y: '-100%',
                            autoAlpha: 0,
                            duration: 1
                            // height: 0
                        }, 0)
                    .to(".banner-card-wrap",
                        {
                            autoAlpha: 1,
                            y: 0,
                            duration: 1
                        }, 0)
                    .to('.home-banner-video',
                        {
                            height: 0
                        }, 1)
                    .to('.banner-wrap .container',
                        {
                            zIndex: 1
                        }, 1)
                    .from('.card-1',
                        {
                            x: '-50%',
                            y: '-50%',
                            autoAlpha: 0
                        }, 1)
                    .from('.card-3',
                        {
                            x: '50%',
                            y: '-50%',
                            autoAlpha: 0
                        }, 1)
                    .from('.card-2',
                        {
                            x: '-0%',
                            y: '-50%',
                            autoAlpha: 0
                        }, 1)
                    .from('.card-4',
                        {
                            x: '-50%',
                            y: '50%',
                            autoAlpha: 0
                        }, 1)
                    .from('.card-5',
                        {
                            x: '50%',
                            y: '50%',
                            autoAlpha: 0
                        }, 1)

            }

            function secondAnim() {
                bannerScrollTl
                    .from('.card-1',
                        {
                            x: '-20%',
                            y: '-20%',
                            autoAlpha: 0
                        }, 0)
                    .from('.card-3',
                        {
                            x: '20%',
                            y: '-20%',
                            autoAlpha: 0
                        }, 0)
                    .from('.card-2',
                        {
                            x: '-0%',
                            y: '-20%',
                            autoAlpha: 0
                        }, 0)
                    .from('.card-4',
                        {
                            x: '-20%',
                            y: '20%',
                            autoAlpha: 0
                        }, 0)
                    .from('.card-5',
                        {
                            x: '20%',
                            y: '20%',
                            autoAlpha: 0
                        }, 0)
            }

            function thirdAnim() {
                bannerScrollTl
                    .to(".banner-content",
                        {
                            scale: 0,
                            autoAlpha: 0
                        }, 0)
                    .to(".banner-card",
                        {
                            scale: 1,
                            autoAlpha: 1
                        }, 0)
                    .to(".card-1",
                        {
                            left: 0,
                            // top: 0
                        }, 0)
                    .to(".card-2",
                        {
                            right: 0,
                            // top: 0,
                            transform: 'scale(1) translateY(0px)'
                        }, 0)
                    .to(".card-3",
                        {
                            left: 0,
                            // top: 0
                        }, 0)
                    .to(".card-4",
                        {
                            right: 0,
                            // bottom: 0
                        }, 0)
                    .to(".card-5",
                        {
                            right: 0,
                            // bottom: 0
                        }, 0)
            }

            function fourthAnim() {
                bannerScrollTl



            }


            // function sixAnim() {
            //     bannerScrollTl
            //         .to(".g-logo",
            //             {
            //                 autoAlpha: 0
            //             }, 0)
            // }

            // ==============anim sequence=====================
            if (i == 0) {
                onloadAnim()            //banner video & header appear 
            } else if (i == 1) {
                firstAnim()
            } else if (i == 2) {
                // secondAnim()
            } else if (i == 3) {
                thirdAnim()
            } else if (i == 4) {
                fourthAnim()
            } else if (i == 5) {
                // fifthAnim()
            } else if (i == 6) {
                // sixAnim()
            } else if (i == 7) {
                // sixAnim()
            }
            // ==============anim sequence=================

        })
        var cardsHide = gsap.timeline({
            scrollTrigger: {
                trigger: ".banner-logo-trigger",
                start: "0% 50%",
                end: "0% -100%",
                // toggleActions: "play none none reverse",
                scrub: true,
                // markers: true,
                // id: "scroll" + i,
            },
        })
        cardsHide
            .to(".five-card-wrap .f-row",
                {
                    scale: 0,
                    autoAlpha: 0
                }, 0)

        var bannerLOGO = gsap.timeline({
            scrollTrigger: {
                trigger: ".banner-logo-trigger",
                start: "0% 50%",
                end: "0% -200%",
                // toggleActions: "play none none reverse",
                scrub: true,
                // markers: true,
                // id: "scroll" + i,
            },
        })
        bannerLOGO
            .to(".g-logo",
                {
                    scale: 8,
                }, 0)
            .to(".orange-bg-sec-wrap",
                {
                    zIndex: 1,
                }, 0)

        var bannerLOGOEnd = gsap.timeline({
            scrollTrigger: {
                trigger: ".show-orange-sec-trigger",
                start: "0% 50%",
                end: "0% 0%",
                // toggleActions: "play none none reverse",
                scrub: true,
                // markers: true,
                // id: "scroll" + i,
            },
        })
        bannerLOGOEnd
            .to(".g-logo",
                {
                    autoAlpha: 0,
                }, 0)
            .to(".orange-bg-sec",
                {
                    autoAlpha: 1,
                    scale: 1
                }, 0)
            .to(".g-logo-wrap",
                {
                    height: 0
                }, 2)



        var projectPinTl = gsap.timeline({
            scrollTrigger: {
                trigger: ".orange-bg-sec-wrap",
                start: `0% 0%`,
                end: "100% 100%",
                pin: ".orange-bg-sec",
                // pin: ".t1",
                scrub: 1,
                // markers: true,
                // id: "pin",
            },
        });

        // bannerScrollTl
        //     .to('.home-banner-video',
        //         {
        //             y: '-100%',
        //             duration: 1,
        //             autoAlpha: 0
        //         }, 0)
        //     .to(".banner-card-wrap",
        //         {
        //             autoAlpha: 1,
        //             y: 0,
        //             duration: 1
        //         }, 1)
        //     .from('.card-1',
        //         {
        //             left: '-50%',
        //             top: '-50%',
        //             duration: 1,
        //             autoAlpha: 0
        //         }, 2)
        //     .from('.card-3',
        //         {
        //             left: '-50%',
        //             top: '50%',
        //             duration: 1,
        //             autoAlpha: 0
        //         }, 2)
        //     .from('.card-2',
        //         {
        //             right: '-50%',
        //             top: '-50%',
        //             duration: 1,
        //             autoAlpha: 0
        //         }, 2)
        //     .from('.card-4',
        //         {
        //             right: '-50%',
        //             top: '50%',
        //             duration: 1,
        //             autoAlpha: 0
        //         }, 2)
        //     .to(".banner-content",
        //         {
        //             scale: 0,
        //             autoAlpha: 0
        //         }, 4)
        //     .to(".banner-card",
        //         {
        //             scale: 1,
        //             autoAlpha: 1

        //         }, 4)
        //     .to(".card-1",
        //         {
        //             left: 0,
        //             top: 0
        //         }, 4)
        //     .to(".card-2",
        //         {
        //             right: 0,
        //             top: 0
        //         }, 4)
        //     .to(".card-3",
        //         {
        //             left: 0,
        //             // top: 0
        //         }, 4)
        //     .to(".card-4",
        //         {
        //             right: 0,
        //             bottom: 0
        //         }, 4)




    })




    // tab animation
    // $('.tab-bg').width($('.tab-nav-wrap li').innerWidth());
    // var bgHeight = $('.tab-nav-wrap li').outerHeight()
    // $('.tab-bg').height(bgHeight);
    // $('.tab-nav-wrap li').click(function () {
    //     var bgTop = $(this).position().top;
    //     $('.tab-bg').css('top', bgTop);
    // })
    // if (window.matchMedia("(min-width: 641px)").matches) {

    //     // areas section pinning
    //     var areaPinTl = gsap.timeline({
    //         scrollTrigger: {
    //             trigger: ".area-height",
    //             start: `0% ${topPadding}`,
    //             end: "100% 0%",
    //             pin: ".area-sticky",
    //             // pin: ".t1",
    //             scrub: 1,
    //             // markers: true,
    //             // id: "area-pin",
    //         },
    //     });

    //     // areas scroll animation
    //     var areaSecHeight = $('.area-tab-wrap').outerHeight();
    //     var areaTrigger = document.querySelectorAll('.area-trigg')

    //     areaTrigger.forEach((section, i) => {

    //         // trigger point dynamic css
    //         $(".area-" + (i + 1)).css({
    //             top: i * (areaSecHeight - 50),
    //         });

    //         // trigger swiper scroll animation
    //         var tabScrollTl = gsap.timeline({
    //             scrollTrigger: {
    //                 trigger: section,
    //                 start: "0% 50%",
    //                 end: "0% 50%",
    //                 // toggleActions: "play none none reverse",
    //                 scrub: 1,
    //                 // markers: true,
    //                 id: "wheel",
    //                 onEnter: () => enterFun(),
    //                 onEnterBack: () => enterBackFun(),
    //             },
    //         })

    //         function enterFun() {
    //             if (i > 0) {
    //                 $('.tab-nav-wrap ul li').eq(i).trigger('click');
    //             }
    //         }

    //         function enterBackFun(e) {
    //             if (i > 0) {
    //                 $('.tab-nav-wrap ul li').eq(i - 1).trigger('click');
    //             }
    //         }

    //     });
    // }



})