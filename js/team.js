// box detail div show hide js 
$(".plus-btn").hover(function () {
    if ($(this).hasClass('active')) {
        $(".team-detail, .plus-btn").removeClass("active");
    } else {
        $(".team-detail, .plus-btn").removeClass("active");
        $(this).addClass("active");
        $(this).parent().parent().find(".team-detail").addClass("active");
    }
});


// teamSwiper Starts

var teamImgSwiper = new Swiper(".teamImgSwiper", {
    slidesPerView: 1,
    speed: 800,
    effect: "fade",
    fadeEffect: {
        crossFade: true,
    },

});

var teamInfoSwiper = new Swiper(".teamInfoSwiper", {
    slidesPerView: 1,
    spaceBetween: 20,
    speed: 800,

    navigation: {
        prevEl: ".team-prev",
        nextEl: ".team-next",
    },
});


teamImgSwiper.controller.control = teamInfoSwiper;
teamInfoSwiper.controller.control = teamImgSwiper;

// teamSwiper Ends