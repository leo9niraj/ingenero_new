$(document).ready(function () {

    // scroll to section (footer)
    // window.onload = () => {
    //     var hashVal = window.location.href.split("#")[1];
    //     if (hashVal != 'undefined') {
    //         var element = document.getElementById(hashVal);
    //         // var headerOffset = 185;
    //         var elementPosition = element.getBoundingClientRect().top;
    //         var offsetPosition = elementPosition + window.pageYOffset;
    //         window.scrollTo({
    //             top: offsetPosition,
    //             behavior: "smooth",
    //         });
    //     }
    // };

    /* casestudy slider */
    var caseSwiper = new Swiper(".caseSwiper", {
        slidesPerView: 3,
        spaceBetween: 30,

        breakpoints: {
            320: {
                slidesPerView: 1.1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            1366: {
                slidesPerView: 3,
                spaceBetween: 40
            }
        }
    });

})