function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}



/* casestudy slider */
/* casestudy slider */
var caseSwiper = new Swiper(".caseSwiper", {
    slidesPerView: 3,
    spaceBetween: 30,

    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        1366: {
            slidesPerView: 3,
            spaceBetween: 40
        }
    }


});




gsap.registerPlugin(ScrollTrigger);
let mm2 = gsap.matchMedia();

/* white label start */
const whiteLabelGsap = gsap.timeline({
    scrollTrigger: {
        trigger: ".culture-sec",
        toggleActions: "restart pause resume none",
        start: "top-=100px 40%",
        end: "bottom+=400px 40%",
        scrub: true,
        // id: '1',
        // markers: true,
        onEnter: labelEnterFn,
    },
});

function labelEnterFn() {
    // console.log("123");
    labelSwiper.autoplay.start();
}

$(".acc-box").click(function () {
    var dataId = $(this).attr("data-id");
    // console.log(dataId);

    labelSwiper.slideTo(dataId);

    $(".acc-box").removeClass("active");
    $(this).addClass("active");
});

const labelSwiper = new Swiper(".labelSwiper", {
    effect: "fade",
    allowTouchMove: false,
    fadeEffect: {
        crossFade: true,
    },
    slidesPerView: 1,
    spaceBetween: 40,
    speed: 1000,
});

labelSwiper.on("slideChange", function () {
    var count = labelSwiper.activeIndex + 1;
    // console.log(count);

    mm2.add("(max-width: 1024px)", () => {
        let i = labelSwiper.activeIndex;
        accSwiper.slideTo(i);
    });

    $(".acc-box").removeClass("active");
    $(".acc-box" + count).addClass("active");

});




mm2.add("(max-width: 1024px)", () => {
    accSwiper = new Swiper(".accSwiper", {
        slidesPerView: 1.1,
        // spaceBetween: 30,
        grabCursor: true,
        centeredSlides: true,
        breakpoints: {
            640: {
                slidesPerView: 1,
            },
        },
    });

    accSwiper.on("slideChange", function () {
        labelSwiper.slideTo(accSwiper.activeIndex);

    });

});



mm2.add("(min-width: 1024px)", () => {
    var solHeight = $('.label-app-left').height()
    $('.label-app-left').height(solHeight + 50)
});