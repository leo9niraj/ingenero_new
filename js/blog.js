$(document).ready(function () {

    /* casestudy slider */
    var caseSwiper = new Swiper(".caseSwiper", {
        slidesPerView: 3,
        spaceBetween: 30,

        breakpoints: {
            320: {
                slidesPerView: 1.1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            1366: {
                slidesPerView: 3,
                spaceBetween: 0
            }
        }
    });
})