// Anim text starts

window.addEventListener("DOMContentLoaded", () => {
    gsap.config({ trialWarn: false });
    // console.clear();
    gsap.registerPlugin(ScrollTrigger, SplitText);
    let split = new SplitText(".anim-text", { type: "lines" });

    function makeItHappen() {
        split.lines.forEach((target) => {
            gsap.to(target, {
                backgroundPositionX: 0,
                ease: "none",
                scrollTrigger: {
                    trigger: target,
                    // markers: true,
                    scrub: 0.5,
                    start: "top 50%",
                    end: "bottom 50%",
                },
                opacity: 1,
            });
        });
    }

    makeItHappen();
});

var counted = 0;
$(window).scroll(function () {
    var oTop = $("#counter").offset().top - window.innerHeight;
    if (counted == 0 && $(window).scrollTop() > oTop) {
        $(".count").each(function () {
            var $this = $(this),
                countTo = $this.attr("data-count");
            $({
                countNum: $this.text(),
            }).animate(
                {
                    countNum: countTo,
                },

                {
                    duration: 2000,
                    easing: "swing",
                    step: function () {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function () {
                        $this.text(this.countNum);
                        //alert('finished');
                    },
                }
            );
        });
        counted = 1;
    }
});


$(document).ready(function () {

    // show first content by default
    $('#tabs-nav li:first-child').addClass('active');
    $('.content').hide();
    $('.content:first').show();

    // click function
    $('#tabs-nav li').click(function () {
        $('#tabs-nav li').removeClass('active');
        $(this).addClass('active');
        $('.content').hide();
        var activeTab = $(this).find('a').attr('href');
        $(activeTab).fadeIn();
        return false;
    });

    // industry detail swiper
    var indDetailSwiper = new Swiper(".indDetailSwiper", {
        direction: "vertical",
        effect: "fade",
        loop: true
    });
    // industry name swiper
    var indNameSwiper = new Swiper(".indNameSwiper", {
        direction: "vertical",
        slidesPerView: 5,
        spaceBetween: 0,
        loop: true,
        centeredSlides: true,
        speed: 5000,
        autoplay: {
            delay: 0,
            disableOnInteraction: true,
        },
        grabCursor: false,
        on: {
            slideChange: function () {
                i = this.realIndex;
                // console.log(i);
                indDetailSwiper.slideTo(i + 1);
            },
        }
    });

    /* casestudy slider */
    var caseSwiper = new Swiper(".caseSwiper", {
        slidesPerView: 3,
        spaceBetween: 30,

        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            1366: {
                slidesPerView: 3,
                spaceBetween: 40
            }
        }


    });

    gsap.registerPlugin(ScrollTrigger);
    gsap.defaults({
        ease: "power2.inOut",
        duration: 1,
    });

    const bannerTrigger = document.querySelectorAll(".banner-trigg");

    // banner section pinning
    var topPadding = $('.main-container').css('padding-top')
    var projectPinTl = gsap.timeline({
        scrollTrigger: {
            trigger: ".banner-height",
            start: `0% ${topPadding}`,
            end: "100% 100%",
            pin: ".banner-sticky",
            // pin: ".t1",
            scrub: 1,
            // markers: true,
            id: "pin",
        },
    });

    var bannerScrollTl = gsap.timeline({
        scrollTrigger: {
            trigger: ".banner-2",
            start: "0% 60%",
            end: "0% 0%",
            // toggleActions: "play none none reverse",
            scrub: 0.5,
            // markers: true,
            id: "scroll",
            onLeave: onLeaveBanner()

        },
    })

    function onLeaveBanner() {
        // console.log('ih');
        $('.gsap-dot').css('position', 'absolute');
        // $('.dot-1').css('top', $('.fold2-dot-1').offset().top)
    }


    bannerScrollTl
        .to(
            ".banner-card",
            {
                scale: 1,
                autoAlpha: 1

            },
            0
        ).to(
            ".banner-content",
            {
                scale: 0,
                autoAlpha: 0
            }, 0)
        .to(".card-1",
            {
                left: 0,
                top: 0
            }, 0)
        .to(".card-2",
            {
                right: 0,
                top: 0
            }, 0)
        .to(".card-3",
            {
                left: 0,
                // top: 0
            }, 0)
        .to(".card-4",
            {
                right: 0,
                bottom: 0
            }, 0)


    // scene animation
    $('.dot-1').css({
        top: $(".banner-dot-1").offset().top,
        left: $(".banner-dot-1").offset().left,
    })
    $('.dot-2').css({
        top: $(".banner-dot-2").offset().top,
        left: $(".banner-dot-2").offset().left,
    })
    $('.dot-3').css({
        top: $(".banner-dot-3").offset().top,
        left: $(".banner-dot-3").offset().left,
    })
    $('.dot-4').css({
        top: $(".banner-dot-1").offset().top,
        left: $(".banner-dot-1").offset().left,
    })
    $('.dot-5').css({
        top: $(".banner-dot-3").offset().top,
        left: $(".banner-dot-3").offset().left,
    })
    $('.dot-6').css({
        top: $(".banner-dot-2").offset().top,
        left: $(".banner-dot-2").offset().left
    })

    bannerScrollTl
        .to(
            ".dot-1",
            {
                top: $('.fold2-dot-1').offset().top,
                left: $('.fold2-dot-1').offset().left,
                width: $('.fold2-dot-1').outerWidth(),
                height: $('.fold2-dot-1').outerHeight()

            }, 0
        ).to(
            ".dot-2",
            {
                top: $('.fold2-dot-2').offset().top,
                left: $('.fold2-dot-2').offset().left,
                width: $('.fold2-dot-2').outerWidth(),
                height: $('.fold2-dot-2').outerHeight()

            }, 0
        ).to(
            ".dot-3",
            {
                top: $('.fold2-dot-3').offset().top,
                left: $('.fold2-dot-3').offset().left,
                width: $('.fold2-dot-3').outerWidth(),
                height: $('.fold2-dot-3').outerHeight()

            }, 0
        ).to(
            ".dot-4",
            {
                top: $('.fold2-dot-4').offset().top,
                left: $('.fold2-dot-4').offset().left,
                width: $('.fold2-dot-4').outerWidth(),
                height: $('.fold2-dot-4').outerHeight(),
                background: 'linear-gradient(46deg, #EC2227 -78.12%, #EC2A25 -52.92%, #EF4123 -13.03%, #F4681E 35.26%, #FA9C18 91.94%, #FDB515 115.04%)'

            }, 0
        ).to(
            ".dot-5",
            {
                top: $('.fold2-dot-5').offset().top,
                left: $('.fold2-dot-5').offset().left,
                width: $('.fold2-dot-5').outerWidth(),
                height: $('.fold2-dot-5').outerHeight(),
                background: 'linear-gradient(46deg, #EC2227 -78.12%, #EC2A25 -52.92%, #EF4123 -13.03%, #F4681E 35.26%, #FA9C18 91.94%, #FDB515 115.04%)'

            }, 0
        ).to(
            ".dot-6",
            {
                top: $('.fold2-dot-6').offset().top,
                left: $('.fold2-dot-6').offset().left,
                width: $('.fold2-dot-6').outerWidth(),
                height: $('.fold2-dot-6').outerHeight(),
                background: 'linear-gradient(46deg,#EC2227-149.04%,#EC2A25-123.79%,#EF4123-83.81%,#F4681E-35.4%,#FA9C18 21.41%,#FDB515 44.56%)'
                // background: '#FDB515'
            }, 0
        )

    let sceneTwo = gsap.timeline({
        scrollTrigger: {
            trigger: ".anim-sec",
            start: "0% 100%",
            end: "100% 00%",
            id: "sceneTwo",
            scrub: true,
            markers: true,
            onEnter: () => enterTwo(),
            onLeave: () => leaveTwo(),
            onEnterBack: () => onEnterBackTwo()
        }
    })

    function enterTwo() {
        setTimeout(() => {

            // $('.gsap-dot').css('position', 'absolute');

            // $('.dot-5').css('left', 'unset')
            // setTimeout(() => {

            $('.dot-1').css('top', $('.fold2-dot-1').offset().top);
            $('.dot-1').css('left', $('.fold2-dot-1').offset().left);
            $('.dot-1').css('background', 'yellow');
            console.log('yep');
        }, 500);
        // }, 2000);
    }

    function leaveTwo() {
        // $('.gsap-dot').css('position', 'absolute');

    }

    function onEnterBackTwo() {
        $('.gsap-dot').css('position', 'fixed');
    }



    sceneTwo
        .from(
            ".dot-1",
            {
                // position: 'absolute'
            }, 0
        )
        .to(
            ".dot-1",
            {
                top: $('.dot1').offset().top,
                left: $('.dot1').offset().left,
                width: $('.dot1').outerWidth(),
                height: $('.dot1').outerHeight(),
            }, 0
        )
    // .to(
    //     [".dot-2", '.dot-3'],
    //     {
    //         top: $('.dot4').offset().top,
    //         left: $('.dot4').offset().left,
    //         width: $('.dot4').outerWidth(),
    //         height: $('.dot4').outerHeight(),
    //     }, 0
    // )
    // .to(
    //     ".dot-4",
    //     {
    //         top: $('.dot2').offset().top,
    //         left: $('.dot2').offset().left,
    //         width: $('.dot2').outerWidth(),
    //         height: $('.dot2').outerHeight(),
    //     }, 0
    // )
    // .to(
    //     ".dot-5",
    //     {
    //         top: $('.dot5').offset().top,
    //         left: $('.dot5').offset().left,
    //         width: $('.dot5').outerWidth(),
    //         height: $('.dot5').outerHeight(),
    //     }, 0
    // )
    // .to(
    //     ".dot-6",
    //     {
    //         top: $('.dot3').offset().top,
    //         left: $('.dot3').offset().left,
    //         width: $('.dot3').outerWidth(),
    //         height: $('.dot3').outerHeight(),
    //     }, 0
    // )
})