$(document).ready(function () {

    // scroll to section (footer)
    // window.onload = () => {
    //     setTimeout(() => {

    //         var hashVal = window.location.href.split("#")[1];
    //         if (hashVal != 'undefined') {
    //             var element = document.getElementById(hashVal);
    //             // var headerOffset = 185;
    //             var elementPosition = element.getBoundingClientRect().top;
    //             var offsetPosition = elementPosition - 200;
    //             console.log(offsetPosition)
    //             window.scrollTo({
    //                 top: offsetPosition,
    //                 behavior: "smooth",
    //             });
    //         }
    //     }, 500);
    // };

    if (window.matchMedia("(min-width: 769px)").matches) {
        // project left swiper
        var projectLeftSwiper = new Swiper(".projectLeftSwiper", {
            direction: "vertical",
            effect: "fade",
            on: {
                slideChange: function () {
                    i = this.realIndex;
                    projectRightSwiper.slideTo(i);
                },
            }
        });

        // project right swiper
        var projectRightSwiper = new Swiper(".projectRightSwiper", {
            direction: "vertical",
            effect: "fade",
        });
    }

    // gsap initialization
    gsap.registerPlugin(ScrollTrigger);
    gsap.defaults({
        ease: "power2.inOut",
        duration: 1,
    });

    let mm = gsap.matchMedia();

    mm.add("(min-width: 769px)", () => {

        var headHeight = $('#header').outerHeight()
        // console.log(headHeight);
        projectTop = headHeight + 80 + 'px'

        if (window.matchMedia("(max-width: 991px)").matches) {
            projectTop = headHeight + 250
        } else if (window.matchMedia("(max-width: 990px)").matches) {
            projectTop = headHeight + 80
        } else if (window.matchMedia("(max-width: 1024px)").matches) {
            projectTop = headHeight + 50
        } else if (window.matchMedia("(max-width: 1024px)").matches) {
            projectTop = headHeight + 50
        } else if (window.matchMedia("(max-width: 1366px)").matches) {
            projectTop = headHeight + 0
        } else if (window.matchMedia("(max-width: 1440px)").matches) {
            projectTop = headHeight + 30
        } else if (window.matchMedia("(max-width: 1600px)").matches) {
            projectTop = headHeight
        }


        const projectTrigger = document.querySelectorAll(".project-trigg");
        var projectHeight = $(".projectLeftSwiper").outerHeight();

        // project section pinning
        var projectPinTl = gsap.timeline({
            scrollTrigger: {
                trigger: ".project-height",
                start: `0% ${projectTop}`,
                end: "100% 100%",
                // end: `100% ${projectTop}`,
                pin: ".project-sticky",
                // pin: ".t1",
                scrub: 1,
                // markers: true,
                id: "pin",
            },
        });

        // orange timeline
        projectPinTl
            .fromTo(
                ".orange-timeline",
                {
                    height: '0%'
                },
                {
                    height: $('.project-box-wrap').outerHeight()
                }
            )

        //  projection section wrapper dynamic height
        var swiperHeight = $('.project-sticky').outerHeight();
        $('.project-height').outerHeight(swiperHeight * projectTrigger.length);

        // project section scroll animation
        projectTrigger.forEach((section, i) => {

            // trigger point dynamic css
            $(".project-" + (i + 1)).css({
                top: i * projectHeight,
            });

            // trigger swiper scroll animation
            var projectScrollTl = gsap.timeline({
                scrollTrigger: {
                    trigger: section,
                    // start: "0% 85%",
                    start: "0% 70%",
                    // end: "100% 40%",
                    end: "100% 00%",
                    scrub: 1,
                    // markers: true,
                    id: "project" + i,
                    onEnter: () => enterFun(),
                    // onEnterBack: () => enterBackFun(),
                    onLeaveBack: () => leaveBackFun()
                },
            })
                .to(
                    '.img-' + i,
                    {
                        scale: 1.2
                    }
                )

            function enterFun() {
                if (i > 1) {
                    projectLeftSwiper.slideTo(i - 1);
                    $('.project-timeline .large-title p').text(i);
                }
            }

            function leaveBackFun() {
                if (i > 1) {
                    $('.project-timeline .large-title p').text(i - 1);
                    projectLeftSwiper.slideTo(i - 2);
                }
            }

            // // orange timeline
            // projectScrollTl
            //     .fromTo(
            //         ".orange-timeline",
            //         {
            //             height: i * 25 + '*'
            //         },
            //         {
            //             height: (i + 1) * 25 + '%'
            //         }
            //     )

        })
    })

})