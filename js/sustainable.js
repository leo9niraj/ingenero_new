$(document).ready(function () {

    // scroll to section (footer)
    // window.onload = () => {
    //     var hashVal = window.location.href.split("#")[1];
    //     if (hashVal != 'undefined') {
    //         var element = document.getElementById(hashVal);
    //         // var headerOffset = 185;
    //         var elementPosition = element.getBoundingClientRect().top;
    //         var offsetPosition = elementPosition + window.pageYOffset;
    //         window.scrollTo({
    //             top: offsetPosition,
    //             behavior: "smooth",
    //         });
    //     }
    // };

    // /* casestudy slider */
    var caseSwiper = new Swiper(".caseSwiper", {
        slidesPerView: 3,
        spaceBetween: 30,

        breakpoints: {
            320: {
                slidesPerView: 1.1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            1366: {
                slidesPerView: 3,
                spaceBetween: 40
            }
        }
    });

    if (window.matchMedia("(min-width: 769px)").matches) {
        // approach swiper
        let autoPlayDelay = 3000;

        let options = {
            init: true,
            loop: false,
            // speed: 1500,
            allowTouchMove: false,
            disableOnInteraction: true,
            autoplay: {
                delay: autoPlayDelay
            },
            effect: "fade",
        };

        let appSwiper = new Swiper('.app-swiper', options);

        var appContentSwiper = new Swiper(".appContentSwiper", {
            init: true,
            loop: false,
            // speed: 1500,
            disableOnInteraction: true,
            allowTouchMove: false,
            autoplay: {
                delay: autoPlayDelay,
                disableOnInteraction: true
            },
            effect: "fade",
            on: {
                slideChange: function () {
                    i = appContentSwiper.activeIndex;
                    $('.approach-wrap .tab-wrap div').removeClass('active');
                    $('.approach-wrap .tab-wrap div').eq(i).addClass('active');
                    appSwiper.slideTo(i);
                },
            }
        });


        $('.approach-wrap .tab-wrap div').click(function () {
            var tabNo = $(this).attr('data-id');
            appContentSwiper.slideTo(tabNo);
            // appContentSwiper.params.disableOnInteraction = true
        })


        // energy solution

        let energyoptions = {
            init: true,
            loop: false,
            // speed: 1500,
            effect: "fade",
            disableOnInteraction: true,

            allowTouchMove: false,
            autoplay: {
                delay: autoPlayDelay,
                disableOnInteraction: true
            },
            on: {
                slideChange: function () {
                    i = energySwiper.activeIndex;
                    $('.energy-wrap .tab-wrap div').removeClass('active');
                    $('.energy-wrap .tab-wrap div').eq(i).addClass('active');
                },
            }
        };

        let energySwiper = new Swiper('.energy-swiper', energyoptions);

        $('.energy-wrap .tab-wrap div').click(function () {
            var tabNo = $(this).attr('data-id');
            energySwiper.slideTo(tabNo);
        })
    }
})