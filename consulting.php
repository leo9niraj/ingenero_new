<?php
/* Template Name: consulting */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>
<!-- Main Container Starts -->
<div class="main-container">

    <!-- banner section -->
    <?php $banner_section_data = get_field('banner_image');
            if ($banner_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="page-hdr">
                <div class="f-row">
                    <div class="w40 w-990-45 w-834-60 w-576-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title">Consulting <br />Services</h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p>Consulting</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-576-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p><?php echo $banner_section_data['title'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-bnr">
                <img src="<?php echo $banner_section_data['banner_image'] ?>" alt="">
            </div>
            <div class="page-big-content">
                <h2 class="large-title"><?php echo $banner_section_data['content'] ?></h2>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- big text
    <section class="">
        <div class="container">

        </div>
    </section> -->

    <!-- consulting section -->
    <section class="comm-section">
        <div class="container">
        <?php if( have_rows('consulting_cards') ): ?>
            <?php while( have_rows('consulting_cards') ): the_row(); ?>
            <div class="learning-wrap l-orange img-text-box img-left">
                <div class="learning-img">
                    <img src="<?php the_sub_field('image'); ?>" alt="">
                </div>
                <div class="learning-content">
                    <h2 class="large-title"><?php the_sub_field('title'); ?></h2>
                    <div class="comm-para">
                        <p><?php the_sub_field('content'); ?></p>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
                <?php endif; ?>


            <!-- <div class="learning-wrap l-orange img-text-box img-left">
                <div class="learning-img">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/cons2.png" alt="">
                </div>
                <div class="learning-content">
                    <h2 class="large-title">Technology Consulting</h2>
                    <div class="comm-para">
                        <p>Embrace the power of technology with our cutting-edge technology consulting services. We
                            help you harness the latest advancements, streamline operations, and implement digital
                            solutions for sustainable growth.</p>
                    </div>
                </div>
            </div>


            <div class="learning-wrap l-orange img-text-box img-left">
                <div class="learning-img">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/cons3.png" alt="">
                </div>
                <div class="learning-content">
                    <h2 class="large-title">Safety Consulting</h2>
                    <div class="comm-para">
                        <p>Safety is paramount. Our safety consulting services ensure a secure working environment,
                            adherence to regulations, and proactive measures to mitigate risks, safeguarding your
                            workforce and operations.</p>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <!-- black overlay img -->
    <section class="comm-section">
        <div class="container">
        <?php if( have_rows('two_bg_cards') ): ?>
            <?php while( have_rows('two_bg_cards') ): the_row(); ?>
            <div class="black-wrap">
                <img src="<?php the_sub_field('banner_image'); ?>" alt="">
                <div class="black-content">
                    <h3 class="small-title white"><?php the_sub_field('title'); ?></h3>
                    <div class="comm-para white">
                        <p><?php the_sub_field('content'); ?></p>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
                <?php endif; ?>
            <!-- <div class="black-wrap">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/bl2.jpg" alt="">
                <div class="black-content">
                    <h3 class="small-title white">Sustainability Consulting</h3>
                    <div class="comm-para white">
                        <p>Embrace sustainability for lasting success. Our sustainability consulting services help
                            you integrate eco-friendly practices, reduce your environmental footprint, and enhance
                            your brand reputation.</p>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white">Want to take your business a step ahead?</h2>
                <div class="comm-para t-center white">
                    <p>Schedule a call with our experts today and find out how we can support you and your company
                        in
                        achieving operational excellence through our tailored insights.</p>

                </div>
                <a href="contact.html" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
</div>
<!-- Main Container Ends -->

<?php get_footer(); ?>