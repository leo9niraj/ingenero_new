<?php
/* Template Name: Sustainable */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>


<!-- Main Container Starts -->
<div class="main-container">

    <!-- banner section -->
    <section class="comm-section no-big-text">
        <div class="container">
            <div class="page-hdr">
                <div class="f-row">
                    <div class="w40 w-990-45 w-834-60 w-576-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title">Sustainable <br />Manufacturing</h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p>Sustainability</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-576-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p><?php echo get_field('title')?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-bnr">
                <img src="<?php echo get_field('banner_image')?>" alt="">
            </div>

        </div>
    </section>

    <!-- solution section -->
    <?php $sustainability_solutions_section_data = get_field('sustainability_solutions_section');
    if ($sustainability_solutions_section_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="safety-study-content-wrap">
                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left solution-left">
                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold">
                                <?php echo $sustainability_solutions_section_data['title'] ?></h2>
                            <div class="comm-para">
                                <p><?php echo $sustainability_solutions_section_data['sub_title'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="safety-content-right solution-right">

                    <?php if( have_rows('sustainability_solutions_section') ):  while( have_rows('sustainability_solutions_section') ): the_row();  ?>
                    <?php if( have_rows('sustainability_solutions_cards') ):  while( have_rows('sustainability_solutions_cards') ): the_row();  ?>
                    <div class="white-box">
                        <div class="solution-img">
                            <img src="<?php the_sub_field('icon'); ?>" alt="">
                        </div>
                        <div class="solution-content">
                            <h2 class="small-title"><?php the_sub_field('title'); ?></h2>
                            <div class="small-para">
                                <p><?php the_sub_field('sub_title'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; endif; 
            endwhile;endif; ?>
                    <!-- <div class="white-box">
                    <div class="solution-img">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/sol2.png" alt="">
                    </div>

                    <div class="solution-content">
                        <h2 class="small-title">Emissions</h2>
                        <div class="small-para">
                            <p>GHG</p>
                        </div>
                    </div>
                </div>

                <div class="white-box">
                    <div class="solution-img">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/sol3.png" alt="">
                    </div>

                    <div class="solution-content">
                        <h2 class="small-title">Yield</h2>
                        <div class="small-para">
                            <p>Resource use efficiency</p>
                        </div>
                    </div>
                </div>

                <div class="white-box">
                    <div class="solution-img">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/sol4.png" alt="">
                    </div>

                    <div class="solution-content">
                        <h2 class="small-title">Safety</h2>
                        <div class="small-para">
                            <p>Process, People</p>
                        </div>
                    </div>
                </div>

                <div class="white-box">
                    <div class="solution-img">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/sol5.png" alt="">
                    </div>

                    <div class="solution-content">
                        <h2 class="small-title">Operational Reliability</h2>
                        <div class="small-para">
                            <p>Profitability, Safety</p>
                        </div>
                    </div>
                </div>

                <div class="white-box">
                    <div class="solution-img">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/sol6.png" alt="">
                    </div>

                    <div class="solution-content">
                        <h2 class="small-title">Environmental Regulations</h2>
                        <div class="small-para">
                            <p>Air, Water, Waste, Material disposal & reuse</p>
                        </div>
                    </div>
                </div>

                <div class="white-box">
                    <div class="solution-img">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/sol7.png" alt="">
                    </div>

                    <div class="solution-content">
                        <h2 class="small-title">Water</h2>
                        <div class="small-para">
                            <p>Usage, Pollutants</p>
                        </div>
                    </div>
                </div> -->

                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- intelligence section -->
    <?php $intelligence_data = get_field('intelligence');
                if ($intelligence_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="learning-wrap lblue-bg">

                <div class="learning-content">
                    <h2 class="sec-title"> <?php echo $intelligence_data['title'] ?></h2>
                    <div class="comm-para">
                        <p><?php echo $intelligence_data['content'] ?></p>
                    </div>
                </div>
                <div class="learning-img">
                    <img src="<?php echo $intelligence_data['banner_image'] ?>" alt="">
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- approach -->
    <?php $ingenero_approach_data = get_field('ingenero_approach_section');
                if ($ingenero_approach_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="approach-wrap">
                <h2 class="sec-title t-center"><?php echo $ingenero_approach_data['main_title'] ?></h2>
                <div class="slider-swiper">
                    <!-- Slider main container -->
                    <div class="tab-wrap med-para desk-tab-wrap">
                        <div class="active progress" data-id="0"><?php echo $ingenero_approach_data['sub_title_1'] ?>
                        </div>
                        <div class="progress mob-tab" data-id="1"><?php echo $ingenero_approach_data['sub_title_2'] ?>
                        </div>
                        <div class="progress mob-tab" data-id="2"><?php echo $ingenero_approach_data['sub_title_3'] ?>
                        </div>
                    </div>
                    <!-- <div class="swiper-progress-bar">
                    <div class="progress"></div>
                    <div class="progress-sections"></div>
                </div> -->
                    <div class="image-content-box">
                        <div class="swiper-container app-swiper">
                            <div class="swiper-wrapper">
                                <div class="tab-wrap med-para mob-tab-wrap">
                                    <div class="progress active" data-id="0">
                                        <?php echo $ingenero_approach_data['sub_title_1'] ?></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="approach-img">
                                        <img src="<?php echo $ingenero_approach_data['image_1'] ?>" alt="">
                                    </div>
                                    <div class="mob-approach-content">
                                        <div class="app-content-card">
                                            <h3 class="large-title"><?php echo $ingenero_approach_data['sub_title_1'] ?>
                                            </h3>
                                            <!-- <ul class="comm-para orange-arrow">
                                            <li>Smart Sustainability Dashboards provide efficient monitoring
                                            </li>
                                            <li>Plan vs Actual” tracking identifies gaps for improvement</li>
                                            <li>Real-time tracking of plant’s total emissions provides
                                                actionable insights for reduction</li>
                                            <li>Data analytics, soft sensors and predictive models provide
                                                timely insights</li>
                                        </ul> -->
                                            <?php echo $ingenero_approach_data['content_1'] ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-wrap med-para mob-tab-wrap">
                                    <div class="progress active" data-id="1">
                                        <?php echo $ingenero_approach_data['sub_title_2'] ?></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="approach-img">
                                        <img src="<?php echo $ingenero_approach_data['image_2'] ?>" alt="">
                                    </div>
                                    <div class="mob-approach-content">
                                        <div class="app-content-card">
                                            <h3 class="large-title"><?php echo $ingenero_approach_data['sub_title_2'] ?>
                                            </h3>
                                            <!-- <ul class="comm-para orange-arrow">
                                            <li>Smart Sustainability Dashboards provide efficient monitoring
                                            </li>
                                            <li>Plan vs Actual” tracking identifies gaps for improvement</li>
                                            <li>Real-time tracking of plant’s total emissions provides
                                                actionable
                                                insights for reduction</li>
                                            <li>Data analytics, soft sensors and predictive models provide
                                                timely
                                                insights</li>
                                        </ul> -->
                                            <?php echo $ingenero_approach_data['content_2'] ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-wrap med-para mob-tab-wrap">
                                    <div class="progress active" data-id="2">
                                        <?php echo $ingenero_approach_data['sub_title_3'] ?></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="approach-img">
                                        <img src="<?php echo $ingenero_approach_data['image_3'] ?>" alt="">
                                    </div>
                                    <div class="mob-approach-content">
                                        <div class="app-content-card">
                                            <h3 class="large-title"><?php echo $ingenero_approach_data['sub_title_3'] ?>
                                            </h3>
                                            <!-- <ul class="comm-para orange-arrow">
                                            <li>Smart Sustainability Dashboards provide efficient monitoring
                                            </li>
                                            <li>Plan vs Actual” tracking identifies gaps for improvement</li>
                                            <li>Real-time tracking of plant’s total emissions provides
                                                actionable insights for reduction</li>
                                            <li>Data analytics, soft sensors and predictive models provide
                                                timely insights</li>
                                        </ul> -->
                                            <?php echo $ingenero_approach_data['content_3'] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper appContentSwiper app-content-swiper">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <div class="app-content-card">
                                        <h3 class="large-title"><?php echo $ingenero_approach_data['sub_title_1'] ?>
                                        </h3>
                                        <!-- <ul class="comm-para orange-arrow">
                                        <li>Smart Sustainability Dashboards provide efficient monitoring</li>
                                        <li>Plan vs Actual” tracking identifies gaps for improvement</li>
                                        <li>Real-time tracking of plant’s total emissions provides actionable
                                            insights for reduction</li>
                                        <li>Data analytics, soft sensors and predictive models provide timely
                                            insights</li>
                                    </ul> -->
                                        <?php echo $ingenero_approach_data['content_1'] ?>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="app-content-card">
                                        <h3 class="large-title"><?php echo $ingenero_approach_data['sub_title_2'] ?>
                                        </h3>
                                        <!-- <ul class="comm-para orange-arrow">
                                        <li>Smart Sustainability Dashboards provide efficient monitoring</li>
                                        <li>Plan vs Actual” tracking identifies gaps for improvement</li>
                                        <li>Real-time tracking of plant’s total emissions provides actionable
                                            insights for reduction</li>
                                        <li>Data analytics, soft sensors and predictive models provide timely
                                            insights</li>
                                    </ul> -->
                                        <?php echo $ingenero_approach_data['content_2'] ?>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="app-content-card">
                                        <h3 class="large-title"><?php echo $ingenero_approach_data['sub_title_3'] ?>
                                        </h3>
                                        <!-- <ul class="comm-para orange-arrow">
                                        <li>Smart Sustainability Dashboards provide efficient monitoring</li>
                                        <li>Plan vs Actual” tracking identifies gaps for improvement</li>
                                        <li>Real-time tracking of plant’s total emissions provides actionable
                                            insights for reduction</li>
                                        <li>Data analytics, soft sensors and predictive models provide timely
                                            insights</li>
                                    </ul> -->
                                        <?php echo $ingenero_approach_data['content_3'] ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- energy -->
    <?php $energy_audit_data = get_field('energy_audit_section');
    if ($energy_audit_data) : ?>
    <section class="comm-section">
        <div class="container">
            <div class="energy-wrap">
                <h2 class="sec-title t-center"><?php echo $energy_audit_data['main_title'] ?></h2>
                <div class="slider-swiper">
                    <div class="tab-wrap med-para desk-tab-wrap">
                        <div class="active progress" data-id="0"><?php echo $energy_audit_data['sub_title_1'] ?></div>
                        <div class="progress mob-tab" data-id="1">PINCH Analysis</div>
                        <div class="progress mob-tab" data-id="2">Process Energy Studies</div>
                    </div>
                    <div class="swiper-container energy-swiper">
                        <div class="swiper-wrapper">
                            <div class="tab-wrap med-para mob-tab-wrap">
                                <div class="progress active" data-id="0"><?php echo $energy_audit_data['sub_title_1'] ?>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="energy-content">
                                    <h3 class="large-title"><?php echo $energy_audit_data['sub_title_1'] ?></h3>
                                    <div class="comm-para">
                                        <!-- <p>Ingenero executes systematic searches for energy conservation
                                        opportunities in a variety of systems, such as:</p> -->

                                        <?php echo $energy_audit_data['content_1'] ?>
                                    </div>
                                    <!-- <ul class="orange-arrow">
                                    <li>Vacuum</li>
                                    <li>Boilers, Furnaces </li>
                                    <li>Systems</li>
                                    <li>Cooling Towers </li>
                                    <li>Pumps, Fans and Blowers </li>
                                    <li>Refrigeration Systems </li>
                                    <li>Burner designs</li>
                                    <li>Compressed Air system </li>
                                    <li>Electrical Equipment</li>
                                    <li>Lighting System, HVAC </li>
                                    <li>Electrical Distribution System</li>
                                    <li>Process equipment</li>
                                </ul> -->
                                    <div class="comm-para">
                                        <!-- <p>The recommendation report assesses current energy consumption compared to
                                        design, identifies energy-saving opportunities in each unit, and
                                        categorizes these opportunities as short-term, medium-term, or long-term
                                        savings.</p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="tab-wrap med-para mob-tab-wrap">
                                <div class="progress active" data-id="1"><?php echo $energy_audit_data['sub_title_2'] ?>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="energy-content">
                                    <h3 class="large-title"><?php echo $energy_audit_data['sub_title_2'] ?></h3>
                                    <div class="comm-para">
                                        <!-- <p>Ingenero has done Pinch analyses for many of its clients. This analysis
                                        technique maximizes process-to-process heat recovery, thereby reducing
                                        external utility loads.</p> -->
                                        <?php echo $energy_audit_data['content_2'] ?>
                                    </div>
                                    <!-- <div class="comm-para">
                                    <p>
                                        The process involves reviewing plant configuration and operation data,
                                        developing a steady-state process model, validating it with actual plant
                                        data, and extracting stream property data. This is followed by a
                                        detailed analysis and a recommendations report for heat exchanger
                                        network design, system changes, and modifications.
                                    </p>
                                </div> -->
                                </div>
                            </div>
                            <div class="tab-wrap med-para mob-tab-wrap">
                                <div class="progress active" data-id="2"><?php echo $energy_audit_data['sub_title_3'] ?>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="energy-content">
                                    <h3 class="large-title"><?php echo $energy_audit_data['sub_title_3'] ?></h3>
                                    <div class="comm-para">
                                        <!-- <p>Optimize your unit operations and processes with Ingenero’s expertise to
                                        reduce your energy needs and carbon footprint.</p> -->
                                        <?php echo $energy_audit_data['content_3'] ?>
                                    </div>
                                    <!-- <ul class="orange-arrow">
                                    <li>Furnace efficiency and heat recovery</li>
                                    <li>Configuration and routing modifications for lower specific
                                        consumptions</li>
                                    <li>Enhanced asset utilization and reliability</li>
                                    <li>Operational improvements</li>
                                    <li>Decoke procedure enhancementsg</li>
                                    <li>Implementation of energy-saving schemes</li>
                                    <li>Reduction of losses during start-ups</li>
                                    <li>Optimization of unit operations in gas plants</li>
                                    <li>Yield modeling and optimization</li>
                                    <li>Utilization of strategic decision tools</li>
                                </ul> -->
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- Relief System section -->
    <section class="comm-section sticky-text-sec">
        <div class="container">
            <?php $i = 1; if( have_rows('energy_audit_cards') ): ?>
            <?php while( have_rows('energy_audit_cards') ): the_row();  ?>
            <div class="safety-study-content-wrap" id="tab-<?php echo $i ?>">
                <div class="sticky-cont-wrapper">
                    <div class="safety-content-left" >
                        <div class="sticky-content">
                            <h2 class="large-title text-semiBold"><?php the_sub_field('title'); ?></h2>
                        </div>
                    </div>
                </div>
                <div class="safety-content-right">
                    <div class="relief-cont-wrap">
                        <!-- <div class="relief-cont">
                        <div class="comm-para">
                            <p><?php the_sub_field('title'); ?>
                            </p>
                        </div>
                    </div> -->
                        <div class="relief-cont">
                            <div class="comm-para">
                                <p><?php the_sub_field('content'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php $i++; endwhile; ?>
            <?php endif; ?>
            <!-- <div class="safety-study-content-wrap">
            <div class="sticky-cont-wrapper">
                <div class="safety-content-left">
                    <div class="sticky-content" id="eneMgmt">
                        <h2 class="large-title text-semiBold">Energy Management System</h2>
                    </div>
                </div>
            </div>
            <div class="safety-content-right">
                <div class="relief-cont-wrap">
                    <div class="relief-cont">
                        <div class="comm-para">
                            <p>Oil and gas companies are increasingly recognizing the urgency of addressing climate change and are making significant strides towards achieving net-zero emissions. Many industry leaders are investing in cleaner energy sources, carbon capture technologies, and sustainable practices to reduce their carbon footprint. Ingenero support this shift with our sustainability services
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="safety-study-content-wrap">
            <div class="sticky-cont-wrapper">
                <div class="safety-content-left">
                    <div class="sticky-content">
                        <h2 class="large-title text-semiBold">Root Cause Analysis (RCA)</h2>
                    </div>
                </div>
            </div>
            <div class="safety-content-right">
                <div class="relief-cont-wrap">
                    <div class="relief-cont">
                        <div class="comm-para">
                            <p>At Ingenero, we excel at solving complex problems by using advanced tools like simulation models and techniques such as barrier, fault tree, and Kepner-Tregoe. Our systematic approach helps us dig deep into operational challenges and pinpoint their root causes with precision. This approach has allowed us to successfully tackle critical issues such as off-spec material production, inadequate output, equipment problems, high energy use, and frequent breakdowns.
                            </p>
                        </div>

                    </div>

                </div>
            </div>

        </div>

        <div class="safety-study-content-wrap">

            <div class="sticky-cont-wrapper">
                <div class="safety-content-left" id="supply">
                    <div class="sticky-content">
                        <h2 class="large-title text-semiBold">Supply Chain Optimization</h2>
                    </div>
                </div>
            </div>

            <div class="safety-content-right">
                <div class="relief-cont-wrap">
                    <div class="relief-cont">
                        <div class="comm-para">
                            <p>At Ingenero, we are experts in optimizing supply chains worldwide. With a strong
                                record in various industries, we offer custom solutions that cut costs and boost
                                operational capacity. Our tools help you analyze logistics and manufacturing,
                                and our Decision Support Models aid in making smart decisions. We are known for
                                solving profit-sharing complexities, consistently saving costs, and being your
                                trusted global partner for supply chain optimization.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        </div>
    </section>

    <!-- more study boxes sec -->
    <!-- <section class="comm-section knowledge-sec">
        <div class="container">
            <div class="sec-header">
                <h2 class="sec-title">Knowledge Base</h2>
                <a href="knowledge-base.html" class="button line">View All</a>
            </div>
            <div class="swiper caseSwiper case-swiper">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <a href="pdf/Oil & Gas/Oil & Gas Case Use Case.pdf" download="">
                            <div class="cs-swiper-box">
                                <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt=""></div>
                                <div class="css-swiper-content">
                                    <div class="cs-swiper-box-btm">
                                        <h3 class="knowledge-card-title">Oil & Gas Case Use Case 1</h3>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="pdf/Petrochemicals/Petrochemicals Use Case1.pdf" download>
                            <div class="cs-swiper-box">
                                <div class="css-swiper-content">
                                    <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt=""></div>
                                    <div class="cs-swiper-box-btm">
                                        <h3 class="knowledge-card-title">Petrochemicals Use Case 1 </h3>
                                    </div>
                                </div>

                            </div>
                        </a>
                    </div>

                    <div class="swiper-slide">
                        <a href="pdf/Power & Utilities/Power & Utilities Case Study1.pdf" download>
                            <div class="cs-swiper-box">
                                <div class="cs-swiper-img"><img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt=""></div>
                                <div class="css-swiper-content">
                                    <div class="cs-swiper-box-btm">
                                        <h3 class="knowledge-card-title">Power & Utilities Case Study 1 </h3>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>


                </div>
            </div>
            <a href="knowledge-base.html" class="button line mob-btn">View All</a>

        </div>
    </section> -->

    <!-- use case -->
    <section class="comm-section">
        <div class="container">
            <div class="usecase-wrap">
                <div class="section-heading">
                    <h2 class="sec-title t-center ">Use Cases</h2>
                </div>

                <div class="usecase-gallery">
                    <div class="usecase-card active">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                            <h3 class="usecase-title">Oil and Gas</h3>

                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#oil">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Oil and Gas</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Petrochemical</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#petro">
                                    <!-- <i class="ph-bold ph-arrow-right"></i> -->
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Petrochemical</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Refining</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#refine">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Refining</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Power And Utilities</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#power">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Power And Utilities</h3>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>



    <?php $pre_footer_section_data = get_field('pre_footer_section');
                if ($pre_footer_section_data) : ?>
    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white"><?php echo $pre_footer_section_data['title'] ?></h2>
                <div class="comm-para t-center white">
                    <p><?php echo $pre_footer_section_data['sub_title'] ?></p>
                </div>
                <a href="<?php echo $pre_footer_section_data['cta'] ?>" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>
<!-- Main Container Ends -->



<?php get_footer(); ?>


<script>
        $(".usecase-card").hover(function () {
            $(".usecase-card").removeClass("active")
            $(this).addClass("active")
        })

        if (window.matchMedia("(max-width: 1024px)").matches) {
            $('.usecase-card').addClass('active')
        }

        /* digital capability sec script */
        var digitalSwiper = new Swiper(".digitalSwiper", {
            spaceBetween: 30,
            slidesPerView: 2.5,
            speed: 800,
            simulateTouch: false,
            allowTouchMove: false,

            scrollbar: {
                el: ".swiper-scrollbar",
            },

            breakpoints: {
                360: {
                    slidesPerView: 1,
                    allowTouchMove: true,
                },
                577: {
                    slidesPerView: 2,
                },
                1025: {
                    slidesPerView: 3,
                },
            },


        });

        let mm = gsap.matchMedia();

        mm.add("(min-width: 577px)", () => {

            const offerTxt = document.querySelectorAll(".trigger-point");
            offerTxt.forEach((section, i) => {
                const tlofferTxt = gsap.timeline({
                    scrollTrigger: {
                        trigger: section,
                        start: "0% 20%",
                        end: "0% 20%",
                        toggleActions: "play none none reverse",
                        onEnter: () => techSlideEnter(i),
                        onEnterBack: () => techSlideEnterBack(i),
                        // markers: true
                    },
                });
            });

            function techSlideEnter(i) {
                digitalSwiper.slideTo(i);
            }

            function techSlideEnterBack(i) {
                digitalSwiper.slideTo(i - 1);
            }

            var swiperTxtGroupCount = document.querySelectorAll(".swiperTxtGroup").length;

            $(".digitals-height-wrap").css({
                "height": (swiperTxtGroupCount * 80) + "vh"
            });

        })

        /* digital page glance sec script */
</script>