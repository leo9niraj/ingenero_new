<?php
/* Template Name: apply-ai */

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blank
 */

get_header(); ?>

<!-- Main Container Starts -->
<div class="main-container">

    <!-- banner -->
    <section class="comm-section">
        <div class="container">
            <div class="page-hdr">
                <div class="f-row">
                    <div class="w30 w-990-45 w-834-60 w-576-100">
                        <div class="page-hdr-left">
                            <h1 class="banner-title">Applied AI</h1>
                            <div class="breadcrumb">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url().'/'?>">Home</a>
                                    </li>
                                    <li>
                                        <p>Applied AI</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="w40 w-576-100">
                        <div class="page-hdr-right">
                            <div class="comm-para">
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-bnr">
                <!-- <img src="<?php bloginfo('template_url'); ?>/assets/img/digitalization.jpg" alt=""> -->
                <div class="ai-banner-video">
                    <video src="<?php bloginfo('template_url'); ?>/assets/video/ai-banner.mp4" autoplay muted playinline loop></video>
                </div>
            </div>

            <div class="page-big-content">
                <h2 class="large-title">Applied AI based digitalization is rapidly transforming the oil and gas
                    ​industry, revolutionizing how companies operate and ​manage their assets. By leveraging
                    advanced ​technologies and innovations enable real-time ​monitoring of equipment, predictive
                    maintenance and ​optimization of operations, ultimately reducing costs ​and minimizing
                    environmental impact.</h2>
            </div>

        </div>
    </section>

    <!-- solution  -->
    <section class="comm-section">
        <div class="container">

            <div class="digitals-height-wrap" id="capab">
                <div class="trigger-point trigger-point1"></div>
                <div class="trigger-point trigger-point2"></div>
                <div class="trigger-point trigger-point3"></div>
                <div class="trigger-point trigger-point4"></div>
                <div class="trigger-point trigger-point5"></div>
                <div class="trigger-point trigger-point6"></div>
                <div class="trigger-point trigger-point7"></div>
                <div class="trigger-point trigger-point8"></div>

                <div class="digitals-sticky">

                    <div class="section-heading">
                        <h2 class="sec-title t-center">Solution Features</h2>
                    </div>

                    <div class="swiper digitalSwiper digital-swiper">

                        <div class="swiper-wrapper">

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-1.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Actionable Intelligence</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Smart analytical engine</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-2.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Smart Dashboards</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Interactive levels</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-3.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Real-time</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Live data solutions</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-4.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Hybrid Models</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>AI/ML + 1st Principle</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-5.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Financial Optimizer</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Cost savings analysis</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-6.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Multi-dimensional</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Covers wide spectrum</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-7.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Data Historian Agnostic</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Wide compatibility</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-8.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">False Positive Resistant</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Real and accurate</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-slide">
                                <div class="text-group swiperTxtGroup">
                                    <div class="white-box digitals-dtl-box">
                                        <div class="digital-icon">
                                            <img src="<?php bloginfo('template_url'); ?>/assets/img/digital-swiper-icon-9.svg" alt="">
                                        </div>

                                        <div class="digital-cotent">
                                            <h2 class="small-title text-medium">Soft sensors</h2>
                                            <div class="safety-cont-para comm-para">
                                                <p>Address drift/anomalies</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="swiper-scrollbar"></div>

                    </div>

                </div>

            </div>




        </div>
    </section>

    <!-- journey -->
    <section class="comm-section">
        <div class="container">
            <div class="journey-wrap">
                <div class="section-heading video-head">
                    <h2 class="sec-title t-center ">Roadmap to Applied AI based Digitalization Journey</h2>
                </div>

                <div class="sec-video">
                    <video src="<?php bloginfo('template_url'); ?>/assets/video/journey.mp4" autoplay loop playsinline muted></video>
                </div>
            </div>
        </div>
    </section>

    <!-- implement -->
    <section class="comm-section">
        <div class="container">
            <div class="implement-wrap">
                <div class="section-heading video-head">
                    <h2 class="sec-title t-center ">Solutions Implemented</h2>
                </div>

                <div class="sec-video">
                    <video src="<?php bloginfo('template_url'); ?>/assets/video/solution.mp4" autoplay loop playsinline muted></video>
                </div>
            </div>
        </div>
    </section>

    <!-- use case -->
    <section class="comm-section">
        <div class="container">
            <div class="usecase-wrap">
                <div class="section-heading">
                    <h2 class="sec-title t-center ">Use Cases</h2>
                </div>

                <div class="usecase-gallery">
                    <div class="usecase-card active">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u1.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                            <h3 class="usecase-title">Oil and Gas</h3>

                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#oil">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Oil and Gas</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u2.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Petrochemical</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#petro">
                                    <!-- <i class="ph-bold ph-arrow-right"></i> -->
                         <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Petrochemical</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u3.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Refining</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#refine">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                   <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Refining</h3>
                        </div>
                    </div>
                    <div class="usecase-card">
                        <div class="usecase-img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/img/u4.jpg" alt="">
                        </div>

                        <div class="usecase-content">
                            <div class="usecase-bottom">
                                <h3 class="usecase-title">Power And Utilities</h3>
                                <!-- <div class="comm-para">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </p>
                                </div> -->
                                <div class="link-arrow">
                                    <a href="<?php echo get_site_url().'/knowledge-base'?>#power">
                                        <!-- <i class="ph-bold ph-arrow-right"></i> -->
                                        <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow--right.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="usecase-content-2">
                            <h3 class="usecase-title">Power And Utilities</h3>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    
     <?php $pre_footer_section_data = get_field('pre_footer_section');
                if ($pre_footer_section_data) : ?>
    <div class="business-banner">
        <div class="container">
            <div class="business-wrap">
                <h2 class="sec-title t-center white"><?php echo $pre_footer_section_data['title'] ?></h2>
                <div class="comm-para t-center white">
                    <p><?php echo $pre_footer_section_data['sub_title'] ?></p>
                </div>
                <a href="<?php echo $pre_footer_section_data['cta'] ?>" class="button white">Contact Us</a>
            </div>
        </div>
    </div>
    <?php endif; ?>


</div>
<!-- Main Container Ends -->




<?php get_footer(); ?>


<script>
        $(".usecase-card").hover(function () {
            $(".usecase-card").removeClass("active")
            $(this).addClass("active")
        })

        if (window.matchMedia("(max-width: 1024px)").matches) {
            $('.usecase-card').addClass('active')
        }

        /* digital capability sec script */
        var digitalSwiper = new Swiper(".digitalSwiper", {
            spaceBetween: 30,
            slidesPerView: 2.5,
            speed: 800,
            simulateTouch: false,
            allowTouchMove: false,

            scrollbar: {
                el: ".swiper-scrollbar",
            },

            breakpoints: {
                360: {
                    slidesPerView: 1,
                    allowTouchMove: true,
                },
                577: {
                    slidesPerView: 2,
                },
                1025: {
                    slidesPerView: 3,
                },
            },


        });

        let mm = gsap.matchMedia();

        mm.add("(min-width: 577px)", () => {

            const offerTxt = document.querySelectorAll(".trigger-point");
            offerTxt.forEach((section, i) => {
                const tlofferTxt = gsap.timeline({
                    scrollTrigger: {
                        trigger: section,
                        start: "0% 20%",
                        end: "0% 20%",
                        toggleActions: "play none none reverse",
                        onEnter: () => techSlideEnter(i),
                        onEnterBack: () => techSlideEnterBack(i),
                        // markers: true
                    },
                });
            });

            function techSlideEnter(i) {
                digitalSwiper.slideTo(i);
            }

            function techSlideEnterBack(i) {
                digitalSwiper.slideTo(i - 1);
            }

            var swiperTxtGroupCount = document.querySelectorAll(".swiperTxtGroup").length;

            if (window.matchMedia("(min-width: 1025px)").matches) {
            $(".digitals-height-wrap").css({
                "height": (swiperTxtGroupCount * 80) + "vh"
            });}

        })

        /* digital page glance sec script */
    </script>